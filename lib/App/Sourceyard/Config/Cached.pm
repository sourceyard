package App::Sourceyard::Config::Cached;
use parent 'App::Sourceyard::Config';

use strict;
use warnings;
use Carp;
use File::stat;
use Storable qw(retrieve store);
use Data::Dumper;

=head1 NAME

App::Sourceyard::Config::Cached - Cacheable version of configuration file parser

=head1 SYNOPSIS

See B<App::Sourceyard::Config>.

=head1 DESCRIPTION

This class extends B<App::Sourceyard::Config> to optionally cache the
parsed configuration object in a disk file. Caching is enabled by passing
the keyword argument

    cache => 1

to the constructor call. To invalidate the existing cache file, use

    cache => 2
    
The name of the cache file is derived from the name of the configuration
file using the following rules:

=over 4

=item Split file path into directory and base name

=item Remove configuration extension from the base name

Recognized configuration extensions are: B<.conf>, B<.cnf>, and B<.cfg>.

=item Append B<.cache> to the resulting base name

=item Prepend dot to the base name

=item Concatenate directory and base names back.

=back

To override this default, the desired cache file name can be supplied using
the B<cachefile> argument to the constructor, e.g.:

    cachefile => '/var/cache/myprog/config'

Additional argument B<rw> can be given to control whether eventual changes
to the configuration introduced by the use of B<set> and B<unset> methods
should be saved in the cache file. Default is

    rw => 0

=cut    

my @KEYS = qw(cache cachefile rw);

sub new {
    my $class = shift;
    my $filename = shift;
    local %_ = @_;
    my %args;
    foreach my $k (@KEYS) {
	$args{"_$k"} = delete $_{$k};
    }
    my $self = $class->SUPER::new($filename, %_);

    if ($args{_cache}) {
	unless ($args{_cachefile}) {
	    my $v = $self->filename;
	    $v =~ s/\.(conf|cnf|cfg)$//;
	    unless ($v =~ s#(.+/)?(.+)#$1.$2#) {
		$v = ".$v";
	    }
	    $args{_cachefile} = "$v.cache";
	}
    }
    $args{_rw} //= 0;
    @{$self}{keys %args} = values %args;
    return $self;
}

sub DESTROY {
    my $self = shift;
    $self->writecache();
}

sub tree {
    my $self = shift;
    my $node = shift // $self->{_conf};
    my $clone = new App::Sourceyard::Config::Node::Section(
	            locus => $node->locus);
    while (my ($k, $n) = each %{$node->subtree}) {
	my $n = $node->subtree($k);
	if ($n->is_section) {
	    my $st = $self->tree($n);
	    $clone->subtree($k => $st) if ($st->keys > 0);
	} elsif (!$n->default) {
	    $clone->subtree($k => new App::Sourceyard::Config::Node(clone => $n));
	}
    }
    return $clone;
}

sub writecache {
    my $self = shift;
    return unless $self->{_cachefile};
    return unless $self->{_conf};
    return unless $self->{_updated};
    $self->debug(1, "storing cache file $self->{_cachefile}");
    store $self->tree, $self->{_cachefile};
}

sub file_up_to_date {
    my ($self, $file) = @_;
    my $st_conf = stat($self->filename) or return 1;
    my $st_file = stat($file)
	or carp "can't stat $file: $!";
    return $st_conf->mtime <= $st_file->mtime;
}

sub parse {
    my $self = shift;

    return if $self->{_conf};
    $self->{_error_count} = 0;

    if ($self->{_cachefile} && -f $self->{_cachefile}) {
	if ($self->{_cache} > 1) {
	    $self->debug(1, "invalidating cache file $self->{_cachefile}");
	} elsif ($self->file_up_to_date($self->{_cachefile})) {
	    my $ref;
	    $self->debug(1, "reading from cache file $self->{_cachefile}");
	    eval { $ref = retrieve($self->{_cachefile}); };
	    if ($self->parse_finish($ref)) {
		$self->{_conf} = $ref;
		$self->{_updated} = $self->{_rw};
		return 1;
	    } elsif ($@) {
		$self->error("warning: unable to load configuration cache: $@");
	    }
	}
	unlink $self->{_cachefile};
    }
    my $ret = $self->SUPER::parse;
    if ($ret) {
	$self->{_updated} = 1;
    }
    return $ret;
}

sub set {
    my $self = shift;
    $self->SUPER::set(@_);
    $self->{_updated} = $self->{_rw};
}

sub unset {
    my $self = shift;
    $self->SUPER::set(@_);
    $self->{_updated} = $self->{_rw};
}

1;    
