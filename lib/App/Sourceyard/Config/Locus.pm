package App::Sourceyard::Config::Locus;

use strict;
use warnings;
use parent 'Exporter';

use Carp;
use Storable qw(dclone);
use Scalar::Util qw(blessed);

=head1 NAME

App::Sourceyard::Config::Locus - source file location

=head1 SYNOPSIS

use App::Sourceyard::Config::Locus;

$locus = new App::Sourceyard::Config::Locus;

$locus = new App::Sourceyard::Config::Locus($file, $line);

$locus->add($file, $line);

$s = $locus->format;

$locus->fixup_names('old' => 'new');

$locus->fixup_lines();

print "$locus: text\n";

$res = $locus1 + $locus2;

=head1 DESCRIPTION

Provides support for manipulating source file locations.    

=head2 $locus = new App::Sourceyard::Config::Locus($file, $line, ...);

Creates a new locus object.  Arguments are optional.  If given, they
indicate the source file name and line numbers this locus is to represent.    
    
=cut

sub new {
    my $class = shift;
    
    my $self = bless { _table => {}, _order => 0 }, $class;

    croak "line numbers not given" if @_ == 1;
    $self->add(@_) if @_ > 1;
    
    return $self;
}

=head2 $obj->clone

Creates a new B<App::Sourceyard::Config::Locus> which is exact copy of B<$obj>.
    
=cut

sub clone {
    my $self = shift;
    return dclone($self);
}

=head2 $locus->add($file, $line, [$line1 ...]);

Adds new location to the locus.  Use this for statements spanning several
lines and/or files.    

Returns B<$locus>.
    
=cut

sub add {
    my ($self, $file) = (shift, shift);
    unless (exists($self->{_table}{$file})) {
	$self->{_table}{$file}{_order} = $self->{_order}++;
	$self->{_table}{$file}{_lines} = [];
    }
    push @{$self->{_table}{$file}{_lines}}, @_;
    delete $self->{_string};
    return $self;
}

=head2 $locus->add_locus($locus2);

Adds locations from B<$locus2> to B<$locus>.

=cut

sub add_locus {
    my ($self, $other) = @_;
    croak "not the same class"
	unless blessed($other) && $other->isa(__PACKAGE__);
    while (my ($file, $tab) = each %{$other->{_table}}) {
	$self->add($file, @{$tab->{_lines}});
    }
    return $self;
}	    

=head2 $s = $locus->format($msg);

Returns a string representation of the locus.  The argument is optional.
If given, its string representation will be concatenated to the formatted
locus with a ": " in between.  If multiple arguments are supplied, their
string representations will be concatenated, separated by horizontal
space characters.  This is useful for formatting error messages.

If the locus contains multiple file locations, the method tries to compact
them by representing contiguous line ranges as B<I<X>-I<Y>> and outputting
each file name once.  Line ranges are separated by commas.  File locations
are separated by semicolons.  E.g.:

    $locus = new App::Sourceyard::Config::Locus("foo", 1);
    $locus->add("foo", 2);
    $locus->add("foo", 3);
    $locus->add("foo", 5);
    $locus->add("bar", 2);
    $locus->add("bar", 7);
    print $locus->format("here it goes");

will produce the following:

    foo:1-3,5;bar:2,7: here it goes

=cut

sub format {
    my $self = shift;
    unless (exists($self->{_string})) {
	$self->{_string} = '';
	foreach my $file (sort {
	                    $self->{_table}{$a}{_order} <=> $self->{_table}{$b}{_order}
			  }
			  keys %{$self->{_table}}) {
	    $self->{_string} .= ';' if $self->{_string};
	    $self->{_string} .= "$file";
	    if (my @lines = @{$self->{_table}{$file}{_lines}}) {
		$self->{_string} .= ':';
		my $beg = shift @lines;
		my $end = $beg;
		my @ranges;
		foreach my $line (@lines) {
		    if ($line == $end + 1) {
			$end = $line;
		    } else {
			if ($end > $beg) {
			    push @ranges, "$beg-$end";
			} else {
			    push @ranges, $beg;
			}
			$beg = $end = $line;
		    }
		}
		
		if ($end > $beg) {
		    push @ranges, "$beg-$end";
		} else {
		    push @ranges, $beg;
		}
		$self->{_string} .= join(',', @ranges);
	    }
	}
    }
    if (@_) {
	if ($self->{_string} ne '') {
	    return "$self->{_string}: " . join(' ', @_);
	} else {
	    return join(' ', @_);
	}
    }
    return $self->{_string};
}

=head2 Overloaded operations

When used in a string, the locus object formats itself. E.g. to print
a diagnostic message one can write:

    print "$locus: some text\n";

In fact, this method is preferred over calling B<$locus->format>.

Two objects can be added:

    $loc1 + $loc2

This will produce a new Locus object containing locations from both B<loc1>
and B<$loc2>.

Moreover, a term can also be a string in the form B<"I<file>:I<line>>:

    $loc + "file:10"

or

    "file:10" + $loc    
    
=cut

use overload
    '""' => sub { shift->format() },
    '+'  => sub {
	my ($self, $other, $swap) = @_;
	if (blessed $other) {
	    return $self->clone->add_locus($other);
        } elsif (!ref($other) && $other =~ m/^(.+):(\d+)$/) {
	    if ($swap) {
		return new App::Sourceyard::Config::Locus($1, $2) + $self;
	    } else {
		return $self->clone->add($1, $2);
	    }
	} else {
	    croak "bad argument type in locus addition";
	}
    };

=head2 $locus->fixup_names('foo' => 'bar', 'baz' => 'quux');

Replaces file names in the locations according to the arguments.

=cut

sub fixup_names {
    my $self = shift;
    local %_ = @_;
    while (my ($oldname, $newname) = each %_) {
	next unless exists $self->{_table}{$oldname};
	croak "target name already exist" if exists $self->{_table}{$newname};
	$self->{_table}{$newname} = delete $self->{_table}{$oldname};
    }
    delete $self->{_string};
}

=head2 $locus->fixup_lines('foo' => 1, 'baz' => -2);

Offsets line numbers for each named file by the given number of lines.  E.g.:

     $locus = new App::Sourceyard::Config::Locus("foo", 1);
     $locus->add("foo", 2);
     $locus->add("foo", 3);
     $locus->add("bar", 3);
     $locus->fixup_lines(foo => 1. bar => -1);
     print $locus->format;

will produce

     foo:2-4,bar:2

If given a single argument, the operation will affect all locations.  E.g.,
adding the following to the example above:

     $locus->fixup_lines(10);
     print $locus->format;

will produce

     foo:22-24;bar:22
    
=cut

sub fixup_lines {
    my $self = shift;
    return unless @_;
    if ($#_ == 0) {
	my $offset = shift;
	while (my ($file, $ref) = each %{$self->{_table}}) {
	    $ref->{_lines} = [map { $_ + $offset } @{$ref->{_lines}}];
	}
    } elsif ($#_ % 2) {
	local %_ = @_;
	while (my ($file, $offset) = each %_) {
	    if (exists($self->{_table}{$file})) {
		$self->{_table}{$file}{_lines} =
		    [map { $_ + $offset }
		         @{$self->{_table}{$file}{_lines}}];
	    }
	}
    } else {
	croak "bad number of arguments";
    }
    delete $self->{_string};
}

1;
