package App::Sourceyard::Config::Node;

use strict;
use warnings;
use parent 'Exporter';
use Clone 'clone';

use Carp;

sub new {
    my $class = shift;
    local %_ = @_;
    my $v;
    my $self;
    if ($v = delete $_{clone}) {
	$self = clone($v);
    } else {
	$self = bless { }, $class;
    }
    if ($v = delete $_{default}) {
	$self->default($v);
    }
    if ($v = delete $_{locus}) {
	$self->locus($v);
    }
    if ($v = delete $_{file}) {
	$self->locus($v, delete $_{line} // 0);
    }
    if ($v = delete $_{order}) {
	$self->order($v);
    }
    croak "unrecognized arguments" if keys(%_);
    return $self;
}

sub locus {
    my $self = shift;
    if (@_ == 1) {
	croak "bad argument type"
	    unless ref($_[0]) eq 'App::Sourceyard::Config::Locus';
	$self->{_locus} = $_[0];
    } elsif (@_ == 2) {
	$self->{_locus} = new App::Sourceyard::Config::Locus(@_);
    } elsif (@_) {
	croak "bad number of arguments";
    }
    return $self->{_locus};
}

sub order {
    my ($self, $val) = @_;
    if (defined($val)) {
	$self->{_order} = $val;
    }
    return $self->{_order} // 0;
}

sub default {
    my ($self, $val) = @_;
    if (defined($val)) {
	$self->{_default} = $val;
    }
    return $self->{_default};
}

sub is_section {
    0
}

sub is_value {
    0
}

1;

	
