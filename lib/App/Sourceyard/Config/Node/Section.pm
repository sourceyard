package App::Sourceyard::Config::Node::Section;
use parent 'App::Sourceyard::Config::Node';
use strict;
use warnings;

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);
    $self->{_subtree} = {};
    return $self;
}

sub subtree {
    my $self = shift;
    if (my $key = shift) {
	if (my $val = shift) {
	    $self->{_subtree}{$key} = $val;
	}
	return $self->{_subtree}{$key};
    }
    return $self->{_subtree};
}

sub keys {
    my $self = shift;
    return keys %{$self->{_subtree}};
}

sub has_key {
    my ($self, $key) = @_;
    return $self->subtree($key);
}

sub delete {
    my ($self, $key) = @_;
    delete $self->{_subtree}{$key};
}

sub is_section {
    1;
}

1;
