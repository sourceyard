package App::Sourceyard::Config::Node::Value;
use parent 'App::Sourceyard::Config::Node';
use strict;
use warnings;

sub new {
    my $class = shift;
    local %_ = @_;
    my $v = delete $_{value};
    my $self = $class->SUPER::new(%_);
    $self->value($v);
    return $self;
}

sub value {
    my ($self, $val) = @_;

    if (defined($val)) {
	$self->{_value} = $val;
	return; # Avoid evaluatig value too early
    } else {
	$val = $self->{_value};
    }
    
    if (ref($val) eq 'CODE') {
	$val = &$val;
    }
    
    return $val;
}

sub is_value {
    1;
}

1;
