package App::Sourceyard::Enum;
use strict;
use warnings;
use Carp;
no strict 'refs';

sub import {
    croak "too few arguments" unless @_ >= 3;
    my ($class, $name, $enum, %opts) = @_;
    my $default = delete $opts{default};
    if ($default) {
	croak "invalid default value" unless exists $enum->{$default};
	$default = $enum->{$default};
    }
    croak "unrecognized arguments" if keys(%opts);
    
    my %VALUES = reverse %$enum;

    * { $name . '::new' } = sub {
	my $class = shift;
	my $self = bless {}, $class;
	if (my $dfl = shift // $default) {
	    croak "invalid default value" unless exists $VALUES{$dfl};
	    $self->{_value} = $dfl;
	}
	local %_ = @_;
	$self->{_readonly} = delete $_{readonly};
	croak "unrecogized arguments" if keys(%_);
	return $self;
    };
    * { $name . '::value' } = sub {
	my ($self, $val) = @_;
	if (defined($val)) {
	    croak "object is readonly" if $self->{_readonly};
	    croak "bad value: $val" unless exists $VALUES{$val};
	    $self->{_value} = $val;
	}
	return $self->{_value};
    };	
    * { $name . '::unset' } = sub {
	my $self = shift;
	croak "object is readonly" if $self->{_readonly};
	$self->{_value} = undef;
    };   
    while (my ($k,$v) = each %$enum) {
	croak "invalid enum value: $k"
	    if $k eq 'new' || $k eq 'value' || $k eq 'unset';
	*{ $name . '::is_' . $k } = sub {
	    my $self = shift;
	    return defined($self->{_value}) && $self->{_value} eq $v;
	};
	*{ $name . '::set_' . $k } = sub {
	    my $self = shift;
	    croak "object is readonly" if $self->{_readonly};
	    $self->{_value} = $v;
	    return $self;
	};
	*{ $name . '::' . $k } = sub { $name->new($v, readonly => 1) };	
    };
    require overload;
    $name->overload::OVERLOAD('""' => sub { shift->value });
}

1;
=head1 NAME

App::Sourceyard::Enum - A factory for generating enumeration classes

=head1 SYNOPSIS

use App::Sourceyard::Enum ('My::Enum' => { white => 0, black => 1 });

$e = new My::Enum(0);

if ($e->is_white) { ... }
$e->set_white;

print "$e";

print $e->value;
$e->value(1);       
$e->is_black;       # returns 1

$e->unset;

$s = My::Enum->white;
$s = My::Enum->black;

=head1 DESCRIPTION

This package is designed to create new classes that represent enumerated types.
The import line syntax is

    use App::Sourceyard::Enum(CLASS => { ENUM }, [DEFAULT]);

Here, I<CLASS> is the new class name, and I<ENUM> is a hash, defining
enumeration type names and corresponding values. Optional I<DEFAULT>
is one of the type names that will be set as default when instantiating
objects of the created class.    

The new class I<CLASS> is created that has methods B<is_I<KEY>> and
B<set_I<KEY>>, for each I<KEY> from I<ENUM>.

=head1 CONSTRUCTOR

=over 4

=item B<new>

Creates an instance of the class having no default value set. In other
words, all B<is_*> methods of the created object return false.

=item B<new(I<VAL>)>

Creates a class instance with I<VAL> as the default value.

=item B<new(I<VAL>, readonly =E<gt> 1)>    

Creates an immutable class instance with I<VAL> as the default value.
Attempts to use B<set_*> methods will result in error.

=back

For each I<KEY> from I<ENUM>, an eponymous constructor is defined, which
returns an immutable object having the value I<KEY>. In other words,

    My::Enum->black

is equivalent to:

    new My::Enum(1, readonly => 1);    
    
=head1 METHODS

=over 4

=item B<is_I<KEY>>

Returns true, if the value of the object is I<KEY>.

=item B<set_I<KEY>>

Sets the object value to I<KEY>.

=item B<value>

Returns the current value of the object.

=item B<value>(I<newval>)

Set new value to the object. In the example above, the following two
statements are equivalent:

        $e->value(1);
        $e->set_black;

=item B<unset>

Unset the value of the object. After this call, B<value> will return
B<undef>, and B<is_I<KEY>> will return 0, for each I<KEY> from I<ENUM>.

=back    

=cut


    

    
