package App::Sourceyard::GPG::PublicKey;

use strict;
use warnings;
use Carp;
use parent 'Exporter';
use DateTime;

=head1 NAME

App::Sourceyard::GPG::PublicKey - GPG public key or subkey

=cut

my @ATTRIBUTES = qw(type
                    validity
                    length
                    algo
                    keyid
                    creation_date
      	            expiry_date
                    serial
                    ownertrust
                    uid
                    sigclass
                    capa);

my %ATTRIBTYPE = (uid => 'ARRAY');

{
    no strict 'refs';
    foreach my $attribute (@ATTRIBUTES) {
	if ($ATTRIBTYPE{$attribute} && $ATTRIBTYPE{$attribute} eq 'ARRAY') {
	    *{ __PACKAGE__ . '::' . $attribute } = sub {
		my $self = shift;
		@{$self->{$attribute}} = @_ if @_;
		return @{$self->{$attribute}};
	    };
	} else {
	    *{ __PACKAGE__ . '::' . $attribute } = sub {
		my $self = shift;
		$self->{$attribute} = shift if @_;
		return $self->{$attribute};
	    }
	};
    }
}

=head1 CONSTRUCTOR

=head2 App::Sourceyard::GPG::PublicKey::new([KW => VAL...])

Takes a list of keyword/value pairs. Allowed keywords are:

=over 4

=item B<all> => [ I<VALS> ]

Sets all attributes. The argument is a reference to the array of values in
the following order: I<type>, I<validity>, I<length>, I<algo>, I<keyid>,
I<creation_date>, I<expiry_date>, I<serial>, I<ownertrust>, I<uid>,
I<sigclass>, I<capa>.  All values are in the same format as returned by
B<gpg --with-colons --with-fingerprint --fixed-list-mode>.    

=item B<blob> => I<STRING>

Sets the ascii-armoured blob.   

=back    

Additionally, the following keywords are recognized: B<type>, B<validity>,
B<length>, B<algo>, B<keyid>, B<creation_date>, B<expiry_date>, B<serial>,
B<ownertrust>, B<uid>, B<sigclass>, B<capa>.
    
=cut
    
sub new {
    my $class = shift;
    my $self = bless {}, $class;
    local %_ = @_;
    my $v;

    if ($v = delete $_{blob}) {
	$self->blob($v);
    }
    if ($v = delete $_{all}) {
	@{$self}{@ATTRIBUTES} = map {
	    defined($_) && $_ eq '' ? undef : $_
	} @$v;
    }
    foreach my $attr (@ATTRIBUTES) {
	if ($v = delete $_{$attr}) {
	    $self->{$attr} = $v;
	}
    }
    $self->{uid} = [ $self->{uid} ] if $self->{uid};
    $self->{creation_date} = _convdate($self->{creation_date});
    $self->{expiry_date} = _convdate($self->{expiry_date});
    
    croak "unrecogized keywords" unless keys(%_) == 0;
    return $self;
}

=head1 METHODS

Accessors and mutators for the following attributes are provided:
B<type>, B<validity>, B<length>, B<algo>, B<keyid>, B<creation_date>,
B<expiry_date>, B<serial>, B<ownertrust>, B<uid>, B<sigclass>, B<capa>.

The calling sequence is:

    $s = $self->ATTR

to get the value, and

    $self->ATTR(NEWVAL)

to set it. Here, ATTR stands for any of the above listed names.   
    
=head2 $s = $self->algoname

Returns the name or numeric code of the encryption algorithm used.
    
=cut

sub algoname {
    my ($self) = @_;
    my %trans = (
	1 => 'RSA',
	16 => 'Elgamal (encrypt only)',
	17 => 'DSA (sign only)',
	20 => 'Elgamal');
    my $c = $self->algo;
    return $trans{$c} || $c;
}

sub _convdate {
    my $datestr = shift;

    return undef unless defined $datestr;

    if ($datestr =~ /^(?<Y>\d{4})
                      (?<m>\d{2})
                      (?<d>\d{2})
                      T
                      (?<H>\d{2})
                      (?<M>\d{2})
                      (?<S>\d{2})$/x) {
	return new DateTime(year => $+{Y},
			    month => $+{m},
			    day => $+{d},
			    hour => $+{H},
			    minute => $+{M},
			    second => $+{S});
    } else {
	return DateTime->from_epoch(epoch => $datestr);
    }
}

=head2 $s = $self->fingerprint([I<VAL>])

Returns or sets the fingerprint.
    
=cut    

sub fingerprint {
    my ($self, $fpr) = @_;
    if ($fpr) {
	$self->{fingerprint} = $fpr;
    }
    return $self->{fingerprint};
}

=head2 $self->addsubkey($pk)

Adds a subkey. B<$pk> must be an instance of App::Sourceyard::GPG::PublicKey
with B<type = sub>.    

=cut

sub addsubkey {
    my ($self, $sk) = @_;
    push @{$self->{subkeys}}, $sk;
}

=head2 $self->subkey([$n])

Returns B<$n>th subkey. Default B<$n> is 0.
    
=cut
    
sub subkey {
    my ($self, $n) = @_;
    $n ||= 0;
    if ($n < 0) {
	$n += @{$self->{subkeys}};
	return undef if $n < 0;
    }
    return undef if $n > $#{$self->{subkeys}};
    return $self->{subkeys}[$n];
}

=head2 $self->adduid($uid)

Appends a user identifier to the list of UIDs.

=cut

sub adduid {
    my ($self, $id) = @_;
    push @{$self->{uid}}, $id;
}

=head2 $self->blob([$str])

Returns or sets the ascii-armored key text.
    
=cut

sub blob {
    my $self = shift;
    $self->{_blob} = shift if @_;
    return $self->{_blob};
}

1;
