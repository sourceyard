package App::Sourceyard::GPG::PublicKeySet;
use strict;
use warnings;
use Carp;
use parent 'Exporter';
use POSIX::Run::Capture;
use POSIX qw(:sys_wait_h);
use App::Sourceyard::GPG::PublicKey;

=head1 NAME

App::Sourceyard::GPG::PublicKeySet - A set of GPG public keys

=cut

our $gpgbin = 'gpg';

use constant {
    PKS_OK => 0,
    PKS_EMPTY => 1,
    PKS_INVALID => 2,
    PKS_SYSERR => 3
};

our @EXPORT = qw(PKS_OK PKS_EMPTY PKS_INVALID PKS_SYSERR);

=head1 CONSTRUCTOR

=head2 new([BLOB])     

Creates a new object. If optional BLOB is supplied, it must contain one
or more GPG public keys in ascii armored form.

A valid object is always created, even if BLOB was invalid or malformed.
The caller must call the B<status> method of the returned object and
analyze its value.    

=cut

sub new {
    my ($class, $key) = @_;
    my $self = bless { _status => PKS_EMPTY, _error => [] }, $class;
    if ($key) {
	$self->blob($key);
    }
    return $self;
}

sub _parse {
    my ($self, $blob) = @_;
    my $out;
    my $cap = new POSIX::Run::Capture(
	argv => [ $gpgbin,
		  '--with-colons',
		  '--with-fingerprint',
		  '--with-fingerprint',
		  '--fixed-list-mode' ],
	stdin => $blob,
        # FIXME: stderr => log
	timeout => 5
    );

    if ($cap->run) {
	if (WIFEXITED($cap->status)) {
	    if (WEXITSTATUS($cap->status) == 0) {
		$self->{_status} = PKS_OK;
	    } else {
		$self->{_status} = PKS_INVALID;
		$self->{_error} = [];
		while (my $s = $cap->next_line(2)) {
		    push @{$self->{_error}}, $s;
		}
	    }
	} elsif (WIFSIGNALED($cap->status)) {
	    $self->{_status} = PKS_SYSERR;
	    $self->{_error} = [
		$gpgbin . " terminated on signal " . WTERMSIG($cap->status)
	    ];
	} else {
	    $self->{_status} = PKS_SYSERR;
	    $self->{_error} = [
		$gpgbin . " terminated with unrecogized code " . $cap->status
	    ];
	}
    } else {
	$self->{_status} = PKS_SYSERR;
	$self->{_error} = [
	    "can't run $gpgbin: " . strerror($cap->errno)
	];
    }

    if ($self->{_status} == PKS_OK) {
	while ($_ = $cap->next_line(1)) {
	    chomp;
	    my @fields = split /:/;
	    my $type = shift @fields;
	    if ($type eq 'pub') {
		push @{$self->{_pubkeys}},
		     new App::Sourceyard::GPG::PublicKey(all => [ $type, @fields ]);
	    } elsif ($type eq 'sub') {
		$self->pubkey(-1)->addsubkey(
		    new App::Sourceyard::GPG::PublicKey(all => [ $type, @fields ])
		);
	    } elsif ($type eq 'fpr') {
		if ($self->pubkey(-1)->subkey) {
		    $self->pubkey(-1)->subkey(-1)->fingerprint($fields[8]);
		} else {
		    $self->pubkey(-1)->fingerprint($fields[8]);
		}
	    } elsif ($type eq 'uid') {
		if ($self->pubkey(-1)->subkey) {
		    $self->pubkey(-1)->subkey(-1)->adduid($fields[8]);
		} else {
		    $self->pubkey(-1)->adduid($fields[8]);
		}
	    }
	}
    }
    
    return $self->{_status};
}

=head1 METHODS

=head2 $self->status    

Returns the status of the object. Possible values are:

=over 4    

=item PKS_OK

The set contains one or more valid public keys.
    
=item PKS_EMPTY

The set is empty.    

=item PKS_INVALID

Supplied blob was not valid.    
    
=item PKS_SYSERR

The B<gpg> program failed.
    
=back
    
=cut

sub status {
    my $self = shift;
    return $self->{_status};
}

=head2 @s = $self->error

Returns an array of error messages. Call this if B<status> returned
B<PKS_INVALID> or B<PKS_SYSERR>.
    
=cut    

sub error {
    my $self = shift;
    return @{$self->{_error}};
}

=head2 $self->clean

Drop all public keys from the set. When this method returns, the object
will have the status B<PKS_EMPTY>.

=cut

sub clean {
    my $self = shift;
    delete $self->{_blob};
    delete $self->{_pubkey};
    $self->{_status} = PKS_EMPTY;
    $self->{_error} = [];
}

=head2 $self->blob([TEXT])

If I<TEXT> is supplied, parses it as one or more armored GPG keys. Returns
B<undef> if parsing failed (the caller should analyze B<status>, then) and
I<TEXT> on success.

Without arguments, returns the current ascii armoured keyset.    
    
=cut    

sub blob {
    my ($self, $newblob) = @_;
    if ($newblob) {
	$self->clean;
	$self->_parse($newblob);
	return undef unless $self->{_status} == PKS_OK;
	$self->{_blob} = $newblob;
	
	my $brx = qr(-----BEGIN PGP PUBLIC KEY BLOCK-----);
	my $erx = qr(-----END PGP PUBLIC KEY BLOCK-----);
	my @lines = split /\r?\n/, $newblob;
	# FIXME: Error handling
	foreach my $key ($self->pubkeys) {
	    my @p;
	    while (defined(my $l = shift @lines)) {
		push @p, $l;
		last if ($l =~ m{^$erx$});
	    }
	    $key->blob(join("\n", @p));
	    while (@lines && $lines[0] !~ m{^$brx$}) {
		shift @lines;
	    }
	}
    }
    return $self->{_blob};
}

=head2 $pk = $self->pubkey([$n])

Returns the B<$n>th public key (an instance of
B<App::Sourceyard::GPG::PublicKey>) from the set. Default B<$n> is 0.

=cut    

sub pubkey {
    my ($self, $n) = @_;
    return undef unless $self->status == PKS_OK;
    $n ||= 0;
    if ($n < 0) {
	$n += @{$self->{_pubkeys}};
	return undef if $n < 0;
    }
    return undef if $n > $#{$self->{_pubkeys}};
    
    return $self->{_pubkeys}[$n];
}

=head2 @pks = $self->pubkeys

Returns all public keys.

=cut    
    
sub pubkeys {
    my ($self) = @_;
    return () unless $self->status == PKS_OK;
    return () unless exists $self->{_pubkeys};
    return @{$self->{_pubkeys}};
}

1;
