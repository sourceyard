package App::Sourceyard::Glob;
use strict;
use warnings;
use parent 'Exporter';
use Carp;

=head1 NAME

App::Sourceyard::Glob - shell-style globbing matcher    

=head1 SYNOPSIS

    $glob = new App::Sourceyard::Glob(@patterns);

    if ($glob->match($text)) {
      ...
    }

    @out = $glob->grep(@in);


=head1 DESCRIPTION

Provides a matcher for shell-style globbing patterns.

=cut    
    
sub glob2regex {
    my @glob = @_;
    return undef unless @glob;
        my %patmap = (
	            '*' => '.*',
	            '?' => '.',
	            '[' => '[',
	            ']' => ']',
	    );
        return '^'
	    . join('|',
		   map { s{(.)} { $patmap{$1} || "\Q$1\E" }gex; "(?:$_)" }
		       @glob)
	    . '$';
}

=head1 CONSTRUCTOR

    $glob = new App::Sourceyard::Glob(@patterns);

Takes a (possibly empty) array of globbing patterns. If the array is empty,
the resulting object will match everything.

=cut

sub new {
    my $class = shift;
    my $self = bless { }, $class;
    if (@_ == 1 && $_[0] !~ /[][*?]/) {
	$self->{_rx} = $_[0];
	$self->{_is_literal} = 1;
    } elsif (@_) {
	$self->{_rx} = glob2regex(@_);
    }
    return $self;
}

sub matches_all {
    my ($self) = @_;
    return ! defined $self->{_rx};
}

sub is_literal {
    my ($self) = @_;
    return $self->{_is_literal};
}

=head1 METHODS

=head2 B<match>

    $bool = $glob->match($input)

Returns true if B<$input> matches any of the globbing patterns used when
creating B<$glob>.

=cut    
    
sub match {
    my ($self, $s) = @_;
    return 1 if $self->matches_all;
    return $s eq $self->{_rx} if $self->is_literal;
    return $s =~ /$self->{_rx}/;
}

=head2 B<grep>

    @out = $glob->grep(@in);

Argument is an array of strings. The method returns an array of strings from
B<@in> which match any of the globbing patterns used when creating B<$glob>.
    
=cut
    
sub grep {
    my $self = shift;
    return @_ if $self->matches_all;
    return grep { $self->match($_) } @_;
}

1;

