package App::Sourceyard::ParseInterval;
use parent 'Exporter';
use Carp;

our @EXPORT = qw(parse_interval);

sub getkey {
    my ($key, $tab) = @_;
    return delete $tab->{$key} if exists $tab->{$key};
    if (my @match = grep { /^$key/ } keys %$tab) {
	getkey($match[0], $tab);
    }
}   

=head1 NAME

App::Sourceyard::ParseInterval - parse textual time interval specification    

=head1 SYNOPSIS

    use App::Sourceyard::ParseInterval;

    $t = parse_interval($text, \$err);
    
=head1 DESCRIPTION

Time interval specification is a string that defines an interval, much the
same way we do this in English: it consists of one or more pairs 
B<I<NUMBER> I<TIME_UNIT>> with optional word B<and> between them. For
example, the following are valid interval specifications:

    1 hour
    2 hours and 35 minutes
    1 year and 7 months 2 weeks 2 days 11 hours and 12 seconds"

The pairs can occur in any order, however unusual it may sound to a human
ear, e.g. I<2 days and 1 year>. Time units can be abbreviated. Last
I<TIME_UNIT> can be omitted, in which case "seconds" are supposed.
    
The module provides a single function for parsing such specification:

    parse_interval($text, \$err)

The interval specification to be parsed is B<$text>. On success, the function
returns the interval in seconds. On error, it returns B<undef> and sets
B<$err> to the part of the input string which caused the error.    
    
=cut

sub parse_interval {
    my %spec = (
	seconds => 1,
	minutes => 60,
	hours => 60 * 60,
	days => 24 * 60 * 60,
	weeks => 7 * 24 * 60 * 60,
	months => 31 * 7 * 24 * 60 * 60,
	years => 356 * 31 * 7 * 24 * 60 * 60
    );

    my @input = split /\s+/, shift;
    my $err = shift;
    my $result;
    while (@input) {
	my $word = lc shift @input;
	if ($word =~ /^\d+$/) {
	    my $term = $word;
	    if ($word = lc shift @input) {
		if (my $n = getkey($word, \%spec)) {
		    $term *= $n;
		} else {
		    if ($err) {
			$$err = join(' ', $word, @input);
		    } else {
			croak "bad time interval near "
			      . join(' ', $word, @input);
		    }
		}
	    }
	    $result += $term;
	    shift @input if (defined($result) && lc($input[0]) eq 'and');
	} else {
	    if ($err) {
		$$err = join(' ', $word, @input);
	    } else {
		croak "bad time interval near " . join(' ', $word, @input);
	    }
	}
    }
    return $result;
}

1;
