package App::Sourceyard::Password;

use strict;
use warnings;
use Carp;
use parent 'Exporter';
use MIME::Base64;
use Crypt::Eksblowfish::Bcrypt qw(bcrypt);
use Crypt::Random::Seed;

=head1 NAME

App::Sourceyard::Password - Encrypted password object

=head1 SYNOPSIS

  use App::Sourceyard::Password;

  $pw = new App::Sourceyard::Password('guessme');
  $pw = new App::Sourceyard::Password(encrypted => '$5$f...');
  
  $pw = new App::Sourceyard::Password;
  $pw->crypt('guessme');

  $encrypted = $pw->crypt;

  if ($pw->crypt($plaintext_pass)) {
    print "OK\n";
  }

=head1 METHODS

=head2 $pw = new App::Sourceyard::Password(...)

Creates new password objects. When called without arguments, the returned
object is not associated with any password. The B<crypt> method should be
called to actually use it. Otherwise, if given a single argument, that
argument is thought to be the password in plaintext. The encrypted value
of that password is computed and stored in the object. Finally, the
constructor can be called with a keyword argument:

    $pw = new App::Sourceyard::Password(encrypted => $enc)

creates a password object and associates the encrypted password B<$enc>
with it, whereas    
    
    $pw = new App::Sourceyard::Password(plaintext => $text)

is equivalent to     

    $pw = new App::Sourceyard::Password($text)
    
=cut

sub new {
    my $class = shift;
    my $self = bless { }, $class;
    if (@_ == 1) {
	$self->crypt(shift);
    } elsif (@_ == 2) {
	if ($_[0] eq 'plain') {
	    $self->crypt($_[1]);
	} elsif ($_[0] eq 'encrypted') {
	    $self->{_hash} = $_[1];
	} else {
	    croak "unrecogized keyword $_[0]";
	}
    } elsif (@_) {
	croak "too many arguments";
    }
    return $self;
}

=head2 $x = $pw->crypt([$text])

Returns the encrypted password value associated with the object. Called with
an argument, encrypts B<$text>, stores it in the object and returns the
encrypted value.

=cut    
    
sub crypt {
    my ($self, $plaintext) = @_;
    if ($plaintext) {
	$self->{_hash} = $self->_encrypt($plaintext);
    }
    return $self->{_hash};
}

=head2 $pw->check($text)

Returns true if B<$text> matches the password stored in B<$pw> and false
otherwise.    

=cut

sub check {
    my ($self, $userpw) = (shift, shift);
    my $cryptpw = shift || $self->crypt;
    if ($cryptpw =~ m{^\$2}) {
	return eval { bcrypt($userpw, $cryptpw) eq $cryptpw };
	return 0;
    } else {
	return CORE::crypt($userpw, $cryptpw) eq $cryptpw;
    }
}

my %encrypt_method = (
    sha512 => { prefix => '$6$rounds=5000$', length => 16 },
    sha256 => { prefix => '$5$', length => 16 },
    blowfish => {
	prefix => '$2a$10$',
	length => 22,
	crypt => sub { return eval { bcrypt($_[0], $_[1]) } }
    },
    md5 => { prefix => '$1$', length => 12 },
    des => { length => 2 }
);

my @encrypt_pref = qw(sha512 sha256 blowfish md5 des);

sub _gensalt {
    my ($self, $length) = @_;
    my $source = new Crypt::Random::Seed(NonBlocking => 1);
    return encode_base64($source->random_bytes($length * 6 / 8));
}

sub _encrypt {
    my ($self, $plaintext) = @_;
    my $hash;
    foreach my $name (@encrypt_pref) {
	my $meth = $encrypt_method{$name} or die "no such method: $name";
	my $prefix = $meth->{prefix} || ''; 
	my $salt = $prefix . $self->_gensalt($meth->{length});
	$hash = &{exists($meth->{crypt}) ? $meth->{crypt} : \&CORE::crypt}
	          ($plaintext, $salt);
	last if substr($hash, 0, length($prefix)) eq $prefix;
    }
    return $hash;
}

sub method {
    my ($self) = @_;

    foreach my $name (@encrypt_pref) {
	my $meth = $encrypt_method{$name} or die "no such method: $name";
	my $prefix = $meth->{prefix} || ''; 
	return $name
	    if substr($self->{_hash}, 0, length($prefix)) eq $prefix;
    }
    return undef;
}

sub upgradable {
    my ($self) = @_;
    return $self->method ne $encrypt_pref[0];
}

1;
