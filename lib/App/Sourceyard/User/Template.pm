package App::Sourceyard::User::Template;

use strict;
use warnings;
use Carp;
use Try::Tiny;
use Email::Address::XS;
use Net::DNS;
use Crypt::Cracklib;
use App::Sourceyard::Password;

use parent 'Exporter';

my @ATTRIBUTES = qw(username realname password email);

sub new {
    my $self = bless {}, shift;
    $self->set(@_);
    return $self;
}

sub set {
    my $self = shift;
    local %_ = @_;
    foreach my $k (@ATTRIBUTES) {
	if (my $v = delete $_{$k}) {
	    $self->$k($v);
	} elsif (!$self->$k) {
	    $self->error($k => 'not defined');
	}
    }
    croak "too many arguments" if keys %_;
}

{
    no strict 'refs';
    use feature 'state';
    foreach my $attribute (@ATTRIBUTES) {
	*{ __PACKAGE__ . '::' . $attribute } = sub {
	    my $self = shift;
	    if (@_) {
		state $in;
		croak "too many arguments" if @_ > 1;
		$self->{$attribute} = shift;
		unless ($in) {
		    $in = 1;
		    $self->status($attribute => undef);
		    $self->error($attribute => undef);
		    $self->${ \ "validate_$attribute" };
		    $in = 0;
	        }
	    }
	    return $self->{$attribute};
	};
	*{ __PACKAGE__ . '::validate_' . $attribute } = sub {
	    my ($self) = @_;
	    if (defined(my $ret = $self->status($attribute))) {
		return $ret;
	    }
	    unless ($self->{$attribute}) {
		$self->error($attribute => 'not defined');
		return $self->status($attribute => 0);
	    }
	    $self->status($attribute => $self->${ \ "_validate_$attribute" });
	};
    }
}

sub status {
    my $self = shift;
    my $attr = shift;
    if (@_ == 1) {
	$self->{_status}{$attr} = shift;
    }
    return $self->{_status}{$attr};
}

sub error {
    my $self = shift;
    if (@_ == 0) {
	return $self->{_errors};
    } elsif (@_ == 1) {
	return $self->{_errors}{$_[0]};
    } elsif (@_ == 2) {
	return $self->{_errors}{$_[0]} = $_[1];
    }
    croak "too many arguments";
}

sub validate {
    my $self = shift;
    foreach my $attribute (@ATTRIBUTES) {
	return 0 unless $self->${ \ "validate_$attribute" };
    }
    return 1;
}

sub _validate_username {
    my ($self) = @_;
    if ($self->username =~ /^[a-zA-Z][a-zA-Z0-9_+-]*$/) {
	if (length($self->username) > 32) {
	    $self->error(username => 'too long');
	} else {
	    return 1;
	}
    }
    $self->error(username => 'invalid login name');
    return 0;
}

sub _validate_realname {
    my ($self) = @_;
    
    if (split(/\s+/, $self->realname) > 1) {
	return 1;
    }
    $self->error(realname => 'invalid real name');
    return 0;
}

sub _validate_email {
    my ($self) = @_;

    my $email = $self->email;
	
    if ($email =~ m{^<(.+)>$}) {
	$email = $1; # dequote
    }
    
    my $addr;
    try {
	$addr = Email::Address::XS->new(address => $email);
    } catch {
	$self->error(email => 'unparseable email address');
	return 0;
    };
    unless (defined($addr->user) && defined($addr->host)) {
	$self->error(email => 'required part missing');
	return 0; 
    }
    
    my $resolver = new Net::DNS::Resolver();
    my $reply = $resolver->query($addr->host, 'MX');
    if (!$reply || $reply->answer == 0) {
	$self->error(email => 'invalid domain');
	return 0;
    }
    $self->email($addr->address);
    return 1;
}

sub _validate_password {
    my ($self) = @_;
    my $pass = $self->password;
    my $res = bad_pass($pass);
    if ($res ne '') {
	$self->error(password => $res);
	return 0;
    }
    $self->password(new App::Sourceyard::Password($pass));
    return 1;
}

1;
