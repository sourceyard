package DBIx::Class::InflateColumn::App::Sourceyard::Enum;
use warnings;
use strict;
use Carp;

sub register_column {
    my ($self, $column, $info, @rest) = @_;
    
    $self->next::method($column, $info, @rest);

    return unless defined $info->{enum} and $info->{enum};

    my $pkg = $info->{enum};
    
    $self->inflate_column(
	$column => {
	    inflate => sub {
		my ($value) = @_;
		my $obj = $pkg->new;
		if (exists($info->{is_nullable}) && $info->{is_nullable}) {
		    $obj->unset;
		}
		if (exists($info->{default_value})) {
		    $obj->value($info->{default_value});
		}
		return $obj;
	    },
	    deflate => sub {
		shift->value
	    }
	}
    );
}

1;
