package Mojolicious::Command::config;
use Mojo::Base 'Mojolicious::Command';
use Getopt::Long qw(GetOptionsFromArray);
use App::Sourceyard::Config qw(:sort);
use App::Sourceyard::Glob;

# Short description
has description => 'Dump configuration values';

# Usage message from SYNOPSIS
has usage => sub { shift->extract_usage };

sub run {
    my $self = shift;
    my $locus;
    my $skip_default;
    my $sort = SORT_NATURAL;
    GetOptionsFromArray(\@_,
			"sort|s=s" => sub {
			    my %s = (
				"no" => NO_SORT,
				"none" => NO_SORT,
				"natural" => SORT_NATURAL,
				"path" => SORT_PATH
				);
			    $sort = $s{$_[1]} || die "bad argument to --sort";
			},
			"locus|l" => \$locus,
			"no-default|skip-default|N" => \$skip_default
	) or exit(1);

    my $glob = new App::Sourceyard::Glob(@_);

    foreach my $x ($self->app->config->flatten(sort => $sort)) {
	my $path = join('.', @{$x->[0]});
	next unless $glob->match($path);
	next if $skip_default && $x->[1]->default;

	my $val = Data::Dumper->new([$x->[1]->value])
	                      ->Useqq(1)->Terse(1)->Indent(0)->Dump;
	$val =~ s{\\@}{@};

	if ($locus) {
	    if ($x->[1]->default) {
		print "(default)";
	    } else {
		print $x->[1]->locus;
	    }
	    print ": ";
	}
	print "$path=$val"."\n";
    }
}

1;

=head1 NAME 

Mojolicious::Command::config - display current configuration
    
=head1 SYNOPSIS

B<sourceyard config> [I<OPTIONS>] [I<PATTERN>...]

=head1 DESCRIPTION

Reads configuration file and dumps its setting to stdout.  One or more
shell-style I<PATTERN>s can be given, in which case only those variable
will be displayed which match one of the patterns.
    
=head1 OPTIONS

=over 4

=item B<-s>, B<--sort=>B<no>|B<none>|B<natural>|B<path>

How to sort output: B<no> (B<none>) - disable sorting, B<natural> - preserve
ordering from the configuration file, B<path> - sort by variable pathnames.

=item B<-l>, B<--locus>

Display source file locations before each variable. Default entries will be
prefixed with the string "B<(default):>".    

=item B<-N>, B<--no-default>, B<--skip-default>

Omit default settings from the output.
    
=back    
    
=cut
