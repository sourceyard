package Mojolicious::Plugin::ACL;
use Mojo::Base 'Mojolicious::Plugin';
use Net::CIDR::Lite;
use FindBin;
use Cwd 'abs_path';
use Data::Dumper;

sub check_acl {
    my ($c, $acl) = @_;
    my $addr = $c->remote_addr;
#    $c->app->log->debug("ADDRESS $addr");
    my $rule;
    foreach $rule (@$acl) {
	if ($rule->{cidr}->find($addr)) {
	    if ($rule->{verb} eq 'allow') {
#		$c->app->log->debug("ALLOW");
		return;
	    }
	    if ($rule->{verb} eq 'deny') {
		$c->render('main/error', text => 'Forbidden', status => 403);
	    }
	}
    }
}

sub register {
    my ($self, $app, $cfg) = @_;

    $app->plugin('RemoteAddr');

    my $file = $cfg->get(qw(core acl));
    if ($file) {
	my @acl;
	my $fd;

	unless ($file =~ m{^/}) {
	    $file = abs_path("$FindBin::Bin/../$file");
	}
	
	unless (open($fd, '<', $file)) {
	    $app->log->fatal("can't open ACL file $file: $!");
	    exit(1);
	}
	my $err;
	while (<$fd>) {
	    chomp;
	    s/^\s+//;
	    next if /^(#.*)?$/;
	    if (/^(?<kw>(?:allow|deny))\s+(?<cidr>\S+)$/) {
		my $s = ($+{cidr} eq 'any' || $+{cidr} eq 'all')
		          ? '0.0.0.0/0' : $+{cidr};
		my $cidr;
		eval { $cidr = Net::CIDR::Lite->new($s) };
		if ($@) {
		    $app->log->fatal("$file:$.: invalid CIDR");
		    $err++;
		} elsif (@acl && $acl[-1]->{verb} eq $+{kw}) {
		    $acl[-1]->{cidr}->add_cidr($cidr);
		} else {
		    push @acl, { verb => $+{kw}, cidr => $cidr };
		}
	    } else {
		$app->log->fatal("$file:$.: unrecognized line");
		$err++;
	    }
	}
	close $fd;
	exit(1) if $err;

	if (@acl) {
	    push @acl, {
		verb => ($acl[-1]->{verb} eq 'allow' ? 'deny' : 'allow'),
		cidr => Net::CIDR::Lite->new('0.0.0.0/0')
	    };
	    
	    foreach my $rule (@acl) {
		$rule->{cidr}->clean;
	    }

	    $app->hook(before_dispatch => sub { check_acl(shift, \@acl) });
	}
    }
}
1;

