use utf8;
package Savane::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';
use Sourceyard::Config;

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:xFEYb/baGHHvhdhfS9Bjfw

sub DBI_arg {
    my ($self, $cfg) = @_;

    $cfg = new Sourceyard::Config unless defined $cfg;
    
    my $dbi = 'DBI:mysql';
    my $v;

    if ($v = $cfg->get(qw(savane database name))) {
	$dbi .= ":database=$v";
    }
    if ($v = $cfg->get(qw(savane database host))) {
	$dbi .= ":host=$v";
    }
    if ($v = $cfg->get(qw(savane database port))) {
	$dbi .= ":port=$v";
    }
    if ($v = $cfg->get(qw(savane database config-file))) {
	$dbi .= ":;mysql_read_default_file=$v";
    }

    return $dbi unless wantarray;
    return ($dbi,
	    $cfg->get(qw(savane database user)),
	    $cfg->get(qw(savane database password)));
}   

sub connect {
    my ($self) = @_;
    $self->SUPER::connect($self->DBI_arg);
}

1;
