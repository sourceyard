use utf8;
package Savane::Schema::Result::Bug;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::Bug

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<bugs>

=cut

__PACKAGE__->table("bugs");

=head1 ACCESSORS

=head2 bug_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 status_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 severity

  data_type: 'integer'
  default_value: 5
  is_nullable: 0

=head2 privacy

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 discussion_lock

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 vote

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 spamscore

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 ip

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 category_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 submitted_by

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 assigned_to

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 date

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 summary

  data_type: 'text'
  is_nullable: 1

=head2 details

  data_type: 'text'
  is_nullable: 1

=head2 close_date

  data_type: 'integer'
  is_nullable: 1

=head2 bug_group_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 resolution_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 category_version_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 platform_version_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 reproducibility_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 size_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 fix_release_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 plan_release_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 hours

  data_type: 'float'
  default_value: 0.00
  is_nullable: 0
  size: [10,2]

=head2 component_version

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 fix_release

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 plan_release

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 priority

  data_type: 'integer'
  default_value: 5
  is_nullable: 0

=head2 planned_starting_date

  data_type: 'integer'
  is_nullable: 1

=head2 planned_close_date

  data_type: 'integer'
  is_nullable: 1

=head2 percent_complete

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 keywords

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 release_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 release

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 originator_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 originator_email

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 originator_phone

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 custom_tf1

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 custom_tf2

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 custom_tf3

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 custom_tf4

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 custom_tf5

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 custom_tf6

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 custom_tf7

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 custom_tf8

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 custom_tf9

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 custom_tf10

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 custom_ta1

  data_type: 'text'
  is_nullable: 0

=head2 custom_ta2

  data_type: 'text'
  is_nullable: 0

=head2 custom_ta3

  data_type: 'text'
  is_nullable: 0

=head2 custom_ta4

  data_type: 'text'
  is_nullable: 0

=head2 custom_ta5

  data_type: 'text'
  is_nullable: 0

=head2 custom_ta6

  data_type: 'text'
  is_nullable: 0

=head2 custom_ta7

  data_type: 'text'
  is_nullable: 0

=head2 custom_ta8

  data_type: 'text'
  is_nullable: 0

=head2 custom_ta9

  data_type: 'text'
  is_nullable: 0

=head2 custom_ta10

  data_type: 'text'
  is_nullable: 0

=head2 custom_sb1

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 custom_sb2

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 custom_sb3

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 custom_sb4

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 custom_sb5

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 custom_sb6

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 custom_sb7

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 custom_sb8

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 custom_sb9

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 custom_sb10

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 custom_df1

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 custom_df2

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 custom_df3

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 custom_df4

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 custom_df5

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "bug_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "status_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "severity",
  { data_type => "integer", default_value => 5, is_nullable => 0 },
  "privacy",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "discussion_lock",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "vote",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "spamscore",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "ip",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "category_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "submitted_by",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "assigned_to",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "date",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "summary",
  { data_type => "text", is_nullable => 1 },
  "details",
  { data_type => "text", is_nullable => 1 },
  "close_date",
  { data_type => "integer", is_nullable => 1 },
  "bug_group_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "resolution_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "category_version_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "platform_version_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "reproducibility_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "size_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "fix_release_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "plan_release_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "hours",
  {
    data_type => "float",
    default_value => "0.00",
    is_nullable => 0,
    size => [10, 2],
  },
  "component_version",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "fix_release",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "plan_release",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "priority",
  { data_type => "integer", default_value => 5, is_nullable => 0 },
  "planned_starting_date",
  { data_type => "integer", is_nullable => 1 },
  "planned_close_date",
  { data_type => "integer", is_nullable => 1 },
  "percent_complete",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "keywords",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "release_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "release",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "originator_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "originator_email",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "originator_phone",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "custom_tf1",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "custom_tf2",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "custom_tf3",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "custom_tf4",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "custom_tf5",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "custom_tf6",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "custom_tf7",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "custom_tf8",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "custom_tf9",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "custom_tf10",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "custom_ta1",
  { data_type => "text", is_nullable => 0 },
  "custom_ta2",
  { data_type => "text", is_nullable => 0 },
  "custom_ta3",
  { data_type => "text", is_nullable => 0 },
  "custom_ta4",
  { data_type => "text", is_nullable => 0 },
  "custom_ta5",
  { data_type => "text", is_nullable => 0 },
  "custom_ta6",
  { data_type => "text", is_nullable => 0 },
  "custom_ta7",
  { data_type => "text", is_nullable => 0 },
  "custom_ta8",
  { data_type => "text", is_nullable => 0 },
  "custom_ta9",
  { data_type => "text", is_nullable => 0 },
  "custom_ta10",
  { data_type => "text", is_nullable => 0 },
  "custom_sb1",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "custom_sb2",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "custom_sb3",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "custom_sb4",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "custom_sb5",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "custom_sb6",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "custom_sb7",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "custom_sb8",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "custom_sb9",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "custom_sb10",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "custom_df1",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "custom_df2",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "custom_df3",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "custom_df4",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "custom_df5",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</bug_id>

=back

=cut

__PACKAGE__->set_primary_key("bug_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:FYY9mMqBXSZ5Qc/KilT1CQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
