use utf8;
package Savane::Schema::Result::BugsCc;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::BugsCc

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<bugs_cc>

=cut

__PACKAGE__->table("bugs_cc");

=head1 ACCESSORS

=head2 bug_cc_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 bug_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 email

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 added_by

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 comment

  data_type: 'text'
  is_nullable: 0

=head2 date

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "bug_cc_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "bug_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "email",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "added_by",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "comment",
  { data_type => "text", is_nullable => 0 },
  "date",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</bug_cc_id>

=back

=cut

__PACKAGE__->set_primary_key("bug_cc_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:FueS5EyKcaUWHEin+mAeNg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
