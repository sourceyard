use utf8;
package Savane::Schema::Result::CookbookContext2recipe;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::CookbookContext2recipe

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<cookbook_context2recipe>

=cut

__PACKAGE__->table("cookbook_context2recipe");

=head1 ACCESSORS

=head2 context_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 recipe_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 audience_anonymous

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 audience_loggedin

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 audience_members

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 audience_technicians

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 audience_managers

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_project

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_homepage

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_cookbook

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_download

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_support

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_bugs

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_task

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_patch

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_news

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_mail

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_cvs

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_arch

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_svn

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_git

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_my

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_stats

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_siteadmin

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 context_people

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 subcontext_browsing

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 subcontext_postitem

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 subcontext_edititem

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 subcontext_search

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 subcontext_configure

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "context_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "recipe_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "audience_anonymous",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "audience_loggedin",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "audience_members",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "audience_technicians",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "audience_managers",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_project",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_homepage",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_cookbook",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_download",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_support",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_bugs",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_task",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_patch",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_news",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_mail",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_cvs",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_arch",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_svn",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_git",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_my",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_stats",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_siteadmin",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "context_people",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "subcontext_browsing",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "subcontext_postitem",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "subcontext_edititem",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "subcontext_search",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "subcontext_configure",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</context_id>

=back

=cut

__PACKAGE__->set_primary_key("context_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:7RcNgyB6VdifgXYVoF5vIA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
