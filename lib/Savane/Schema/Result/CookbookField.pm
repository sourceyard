use utf8;
package Savane::Schema::Result::CookbookField;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::CookbookField

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<cookbook_field>

=cut

__PACKAGE__->table("cookbook_field");

=head1 ACCESSORS

=head2 bug_field_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 field_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 display_type

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 display_size

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 label

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 description

  data_type: 'text'
  is_nullable: 0

=head2 scope

  data_type: 'char'
  default_value: (empty string)
  is_nullable: 0
  size: 1

=head2 required

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 empty_ok

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 keep_history

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 special

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 custom

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "bug_field_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "field_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "display_type",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "display_size",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "label",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "description",
  { data_type => "text", is_nullable => 0 },
  "scope",
  { data_type => "char", default_value => "", is_nullable => 0, size => 1 },
  "required",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "empty_ok",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "keep_history",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "special",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "custom",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</bug_field_id>

=back

=cut

__PACKAGE__->set_primary_key("bug_field_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:FMfEY52Sj1i4seGMuus7YA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
