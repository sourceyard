use utf8;
package Savane::Schema::Result::CookbookFieldValue;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::CookbookFieldValue

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<cookbook_field_value>

=cut

__PACKAGE__->table("cookbook_field_value");

=head1 ACCESSORS

=head2 bug_fv_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 bug_field_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 value_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 value

  data_type: 'text'
  is_nullable: 0

=head2 description

  data_type: 'text'
  is_nullable: 0

=head2 order_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 status

  data_type: 'char'
  default_value: 'A'
  is_nullable: 0
  size: 1

=head2 email_ad

  data_type: 'text'
  is_nullable: 1

=head2 send_all_flag

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "bug_fv_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "bug_field_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "value_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "value",
  { data_type => "text", is_nullable => 0 },
  "description",
  { data_type => "text", is_nullable => 0 },
  "order_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "status",
  { data_type => "char", default_value => "A", is_nullable => 0, size => 1 },
  "email_ad",
  { data_type => "text", is_nullable => 1 },
  "send_all_flag",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</bug_fv_id>

=back

=cut

__PACKAGE__->set_primary_key("bug_fv_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:OJdptVLFes0E8kf0WDQMNw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
