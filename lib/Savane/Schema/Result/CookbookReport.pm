use utf8;
package Savane::Schema::Result::CookbookReport;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::CookbookReport

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<cookbook_report>

=cut

__PACKAGE__->table("cookbook_report");

=head1 ACCESSORS

=head2 report_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 80

=head2 description

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 scope

  data_type: 'varchar'
  default_value: 'I'
  is_nullable: 0
  size: 3

=cut

__PACKAGE__->add_columns(
  "report_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "user_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 80 },
  "description",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "scope",
  { data_type => "varchar", default_value => "I", is_nullable => 0, size => 3 },
);

=head1 PRIMARY KEY

=over 4

=item * L</report_id>

=back

=cut

__PACKAGE__->set_primary_key("report_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:36C9NtvwyM9sizA3tjrvug


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
