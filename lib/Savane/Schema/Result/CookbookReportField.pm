use utf8;
package Savane::Schema::Result::CookbookReportField;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::CookbookReportField

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<cookbook_report_field>

=cut

__PACKAGE__->table("cookbook_report_field");

=head1 ACCESSORS

=head2 report_id

  data_type: 'integer'
  default_value: 100
  is_nullable: 0

=head2 field_name

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 show_on_query

  data_type: 'integer'
  is_nullable: 1

=head2 show_on_result

  data_type: 'integer'
  is_nullable: 1

=head2 place_query

  data_type: 'integer'
  is_nullable: 1

=head2 place_result

  data_type: 'integer'
  is_nullable: 1

=head2 col_width

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "report_id",
  { data_type => "integer", default_value => 100, is_nullable => 0 },
  "field_name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "show_on_query",
  { data_type => "integer", is_nullable => 1 },
  "show_on_result",
  { data_type => "integer", is_nullable => 1 },
  "place_query",
  { data_type => "integer", is_nullable => 1 },
  "place_result",
  { data_type => "integer", is_nullable => 1 },
  "col_width",
  { data_type => "integer", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:poT5//d30fJu+0Z1e5pDAg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
