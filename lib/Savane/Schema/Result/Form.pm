use utf8;
package Savane::Schema::Result::Form;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::Form

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<form>

=cut

__PACKAGE__->table("form");

=head1 ACCESSORS

=head2 form_id

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 timestamp

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "form_id",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "timestamp",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "user_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:xs8hhKdd4zmC7wKv1jlxGQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
