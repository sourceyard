use utf8;
package Savane::Schema::Result::Forum;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::Forum

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<forum>

=cut

__PACKAGE__->table("forum");

=head1 ACCESSORS

=head2 msg_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 group_forum_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 posted_by

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 subject

  data_type: 'text'
  is_nullable: 0

=head2 body

  data_type: 'text'
  is_nullable: 0

=head2 date

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_followup_to

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 thread_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 has_followups

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "msg_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "group_forum_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "posted_by",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "subject",
  { data_type => "text", is_nullable => 0 },
  "body",
  { data_type => "text", is_nullable => 0 },
  "date",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_followup_to",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "thread_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "has_followups",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</msg_id>

=back

=cut

__PACKAGE__->set_primary_key("msg_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:oRdGN4M8dnmHhi1sVYtsVw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
