use utf8;
package Savane::Schema::Result::ForumSavedPlace;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::ForumSavedPlace

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<forum_saved_place>

=cut

__PACKAGE__->table("forum_saved_place");

=head1 ACCESSORS

=head2 saved_place_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 forum_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 save_date

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "saved_place_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "forum_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "save_date",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</saved_place_id>

=back

=cut

__PACKAGE__->set_primary_key("saved_place_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:jaqRJTDQc01keaz5pNqOCA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
