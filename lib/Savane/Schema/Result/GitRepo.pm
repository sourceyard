use utf8;
package Savane::Schema::Result::GitRepo;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::GitRepo

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<git_repo>

=cut

__PACKAGE__->table("git_repo");

=head1 ACCESSORS

=head2 repo_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 master

  data_type: 'enum'
  extra: {list => ["Y","N"]}
  is_nullable: 1

=head2 owner

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 group_id

  data_type: 'integer'
  is_nullable: 1

=head2 description

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 readme

  data_type: 'text'
  is_nullable: 1

=head2 readme_html

  data_type: 'text'
  is_nullable: 1

=head2 updated

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  default_value: '0000-00-00 00:00:00'
  is_nullable: 0

=head2 synchronized

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  default_value: '0000-00-00 00:00:00'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "repo_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "master",
  { data_type => "enum", extra => { list => ["Y", "N"] }, is_nullable => 1 },
  "owner",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "group_id",
  { data_type => "integer", is_nullable => 1 },
  "description",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "readme",
  { data_type => "text", is_nullable => 1 },
  "readme_html",
  { data_type => "text", is_nullable => 1 },
  "updated",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    default_value => "0000-00-00 00:00:00",
    is_nullable => 0,
  },
  "synchronized",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    default_value => "0000-00-00 00:00:00",
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</repo_id>

=back

=cut

__PACKAGE__->set_primary_key("repo_id");

=head1 UNIQUE CONSTRAINTS

=head2 C<group_id_2>

=over 4

=item * L</group_id>

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("group_id_2", ["group_id", "name"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:SVv7Ihf8sQWzJolJUB1G5w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
