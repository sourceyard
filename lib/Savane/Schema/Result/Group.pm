use utf8;
package Savane::Schema::Result::Group;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::Group

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<groups>

=cut

__PACKAGE__->table("groups");

=head1 ACCESSORS

=head2 group_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 group_name

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 type

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 is_public

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 status

  data_type: 'char'
  default_value: 'A'
  is_nullable: 0
  size: 1

=head2 unix_group_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 30

=head2 gidnumber

  data_type: 'integer'
  is_nullable: 1

=head2 short_description

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 license

  data_type: 'varchar'
  is_nullable: 1
  size: 16

=head2 register_purpose

  data_type: 'text'
  is_nullable: 1

=head2 required_software

  data_type: 'text'
  is_nullable: 1

=head2 other_comments

  data_type: 'text'
  is_nullable: 1

=head2 license_other

  data_type: 'text'
  is_nullable: 1

=head2 register_time

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 rand_hash

  data_type: 'text'
  is_nullable: 1

=head2 registered_gpg_keys

  data_type: 'text'
  is_nullable: 1

=head2 use_homepage

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_mail

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_patch

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_task

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_forum

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_cvs

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_arch

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_svn

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_git

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_hg

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_bzr

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_news

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_support

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_download

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_bugs

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 use_extralink_documentation

  data_type: 'char'
  default_value: 0
  is_nullable: 1
  size: 1

=head2 bugs_preamble

  data_type: 'text'
  is_nullable: 1

=head2 task_preamble

  data_type: 'text'
  is_nullable: 1

=head2 patch_preamble

  data_type: 'text'
  is_nullable: 1

=head2 support_preamble

  data_type: 'text'
  is_nullable: 1

=head2 cookbook_preamble

  data_type: 'text'
  is_nullable: 1

=head2 long_description

  data_type: 'text'
  is_nullable: 1

=head2 devel_status

  data_type: 'varchar'
  default_value: 0
  is_nullable: 1
  size: 5

=head2 url_homepage

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_download

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_forum

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_support

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_mail

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_cvs

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_cvs_viewcvs

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_cvs_viewcvs_homepage

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_arch

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_arch_viewcvs

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_svn

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_svn_viewcvs

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_git

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_git_viewcvs

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_hg

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_hg_viewcvs

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_bzr

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_bzr_viewcvs

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_bugs

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_task

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_patch

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 url_extralink_documentation

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 dir_cvs

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 dir_arch

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 dir_svn

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 dir_git

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 dir_hg

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 dir_bzr

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 dir_homepage

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 dir_download

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 new_bugs_address

  data_type: 'text'
  is_nullable: 0

=head2 new_patch_address

  data_type: 'text'
  is_nullable: 0

=head2 new_support_address

  data_type: 'text'
  is_nullable: 0

=head2 new_task_address

  data_type: 'text'
  is_nullable: 0

=head2 new_news_address

  data_type: 'text'
  is_nullable: 0

=head2 new_cookbook_address

  data_type: 'text'
  is_nullable: 0

=head2 bugs_glnotif

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 support_glnotif

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 task_glnotif

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 patch_glnotif

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 cookbook_glnotif

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 send_all_bugs

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 send_all_patch

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 send_all_support

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 send_all_task

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 send_all_cookbook

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 bugs_private_exclude_address

  data_type: 'text'
  is_nullable: 1

=head2 task_private_exclude_address

  data_type: 'text'
  is_nullable: 1

=head2 support_private_exclude_address

  data_type: 'text'
  is_nullable: 1

=head2 patch_private_exclude_address

  data_type: 'text'
  is_nullable: 1

=head2 cookbook_private_exclude_address

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "group_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "group_name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "type",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "is_public",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "status",
  { data_type => "char", default_value => "A", is_nullable => 0, size => 1 },
  "unix_group_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 30 },
  "gidnumber",
  { data_type => "integer", is_nullable => 1 },
  "short_description",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "license",
  { data_type => "varchar", is_nullable => 1, size => 16 },
  "register_purpose",
  { data_type => "text", is_nullable => 1 },
  "required_software",
  { data_type => "text", is_nullable => 1 },
  "other_comments",
  { data_type => "text", is_nullable => 1 },
  "license_other",
  { data_type => "text", is_nullable => 1 },
  "register_time",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "rand_hash",
  { data_type => "text", is_nullable => 1 },
  "registered_gpg_keys",
  { data_type => "text", is_nullable => 1 },
  "use_homepage",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_mail",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_patch",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_task",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_forum",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_cvs",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_arch",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_svn",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_git",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_hg",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_bzr",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_news",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_support",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_download",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_bugs",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "use_extralink_documentation",
  { data_type => "char", default_value => 0, is_nullable => 1, size => 1 },
  "bugs_preamble",
  { data_type => "text", is_nullable => 1 },
  "task_preamble",
  { data_type => "text", is_nullable => 1 },
  "patch_preamble",
  { data_type => "text", is_nullable => 1 },
  "support_preamble",
  { data_type => "text", is_nullable => 1 },
  "cookbook_preamble",
  { data_type => "text", is_nullable => 1 },
  "long_description",
  { data_type => "text", is_nullable => 1 },
  "devel_status",
  { data_type => "varchar", default_value => 0, is_nullable => 1, size => 5 },
  "url_homepage",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_download",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_forum",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_support",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_mail",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_cvs",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_cvs_viewcvs",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_cvs_viewcvs_homepage",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_arch",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_arch_viewcvs",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_svn",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_svn_viewcvs",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_git",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_git_viewcvs",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_hg",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_hg_viewcvs",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_bzr",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_bzr_viewcvs",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_bugs",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_task",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_patch",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "url_extralink_documentation",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "dir_cvs",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "dir_arch",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "dir_svn",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "dir_git",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "dir_hg",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "dir_bzr",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "dir_homepage",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "dir_download",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "new_bugs_address",
  { data_type => "text", is_nullable => 0 },
  "new_patch_address",
  { data_type => "text", is_nullable => 0 },
  "new_support_address",
  { data_type => "text", is_nullable => 0 },
  "new_task_address",
  { data_type => "text", is_nullable => 0 },
  "new_news_address",
  { data_type => "text", is_nullable => 0 },
  "new_cookbook_address",
  { data_type => "text", is_nullable => 0 },
  "bugs_glnotif",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "support_glnotif",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "task_glnotif",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "patch_glnotif",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "cookbook_glnotif",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "send_all_bugs",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "send_all_patch",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "send_all_support",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "send_all_task",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "send_all_cookbook",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "bugs_private_exclude_address",
  { data_type => "text", is_nullable => 1 },
  "task_private_exclude_address",
  { data_type => "text", is_nullable => 1 },
  "support_private_exclude_address",
  { data_type => "text", is_nullable => 1 },
  "patch_private_exclude_address",
  { data_type => "text", is_nullable => 1 },
  "cookbook_private_exclude_address",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</group_id>

=back

=cut

__PACKAGE__->set_primary_key("group_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:NiyLBaEo9chB4ZvB/ncMog


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
