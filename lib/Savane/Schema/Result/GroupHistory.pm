use utf8;
package Savane::Schema::Result::GroupHistory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::GroupHistory

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<group_history>

=cut

__PACKAGE__->table("group_history");

=head1 ACCESSORS

=head2 group_history_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 field_name

  data_type: 'text'
  is_nullable: 0

=head2 old_value

  data_type: 'text'
  is_nullable: 0

=head2 mod_by

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 date

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "group_history_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "field_name",
  { data_type => "text", is_nullable => 0 },
  "old_value",
  { data_type => "text", is_nullable => 0 },
  "mod_by",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "date",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</group_history_id>

=back

=cut

__PACKAGE__->set_primary_key("group_history_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:4R7cXcLPP5mDf9lqG2cQ9Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
