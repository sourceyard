use utf8;
package Savane::Schema::Result::GroupPreference;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::GroupPreference

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<group_preferences>

=cut

__PACKAGE__->table("group_preferences");

=head1 ACCESSORS

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 preference_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 preference_value

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "preference_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "preference_value",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</group_id>

=item * L</preference_name>

=back

=cut

__PACKAGE__->set_primary_key("group_id", "preference_name");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:REuT9fnwc5h7TZJCgYYkDw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
