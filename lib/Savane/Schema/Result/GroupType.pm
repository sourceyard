use utf8;
package Savane::Schema::Result::GroupType;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::GroupType

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<group_type>

=cut

__PACKAGE__->table("group_type");

=head1 ACCESSORS

=head2 type_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 admin_email_adress

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 base_host

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 mailing_list_host

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 can_use_homepage

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 can_use_download

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 can_use_cvs

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 can_use_arch

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 can_use_svn

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 can_use_git

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 can_use_hg

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 can_use_bzr

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 can_use_license

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 can_use_devel_status

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 can_use_forum

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 can_use_mailing_list

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 can_use_patch

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 can_use_task

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 can_use_news

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 can_use_support

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 can_use_bug

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 is_menu_configurable_homepage

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_download

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_forum

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_support

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_mail

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_cvs

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_cvs_viewcvs

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_cvs_viewcvs_homepage

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_arch

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_arch_viewcvs

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_svn

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_svn_viewcvs

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_git

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_git_viewcvs

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_hg

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_hg_viewcvs

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_bzr

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_bzr_viewcvs

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_bugs

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_task

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_patch

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_menu_configurable_extralink_documentation

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_configurable_download_dir

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 homepage_scm

  data_type: 'varchar'
  default_value: 'cvs'
  is_nullable: 0
  size: 25

=head2 dir_type_cvs

  data_type: 'varchar'
  default_value: 'basic'
  is_nullable: 0
  size: 255

=head2 dir_type_arch

  data_type: 'varchar'
  default_value: 'basic'
  is_nullable: 0
  size: 255

=head2 dir_type_svn

  data_type: 'varchar'
  default_value: 'basic'
  is_nullable: 0
  size: 255

=head2 dir_type_git

  data_type: 'varchar'
  default_value: 'basic'
  is_nullable: 0
  size: 255

=head2 dir_type_hg

  data_type: 'varchar'
  default_value: 'basic'
  is_nullable: 0
  size: 255

=head2 dir_type_bzr

  data_type: 'varchar'
  default_value: 'basic'
  is_nullable: 0
  size: 255

=head2 dir_type_homepage

  data_type: 'varchar'
  default_value: 'basic'
  is_nullable: 0
  size: 255

=head2 dir_type_download

  data_type: 'varchar'
  default_value: 'basic'
  is_nullable: 0
  size: 255

=head2 dir_homepage

  data_type: 'varchar'
  default_value: '/'
  is_nullable: 1
  size: 255

=head2 dir_cvs

  data_type: 'varchar'
  default_value: '/'
  is_nullable: 1
  size: 255

=head2 dir_arch

  data_type: 'varchar'
  default_value: '/'
  is_nullable: 1
  size: 255

=head2 dir_svn

  data_type: 'varchar'
  default_value: '/'
  is_nullable: 1
  size: 255

=head2 dir_git

  data_type: 'varchar'
  default_value: '/'
  is_nullable: 0
  size: 255

=head2 dir_hg

  data_type: 'varchar'
  default_value: '/'
  is_nullable: 0
  size: 255

=head2 dir_bzr

  data_type: 'varchar'
  default_value: '/'
  is_nullable: 0
  size: 255

=head2 dir_download

  data_type: 'varchar'
  default_value: '/'
  is_nullable: 1
  size: 255

=head2 url_homepage

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_download

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_cvs_viewcvs

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_arch_viewcvs

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_svn_viewcvs

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_git_viewcvs

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 0
  size: 255

=head2 url_hg_viewcvs

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 0
  size: 255

=head2 url_bzr_viewcvs

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 0
  size: 255

=head2 url_cvs_viewcvs_homepage

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_mailing_list_listinfo

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_mailing_list_subscribe

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_mailing_list_unsubscribe

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_mailing_list_archives

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_mailing_list_archives_private

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_mailing_list_admin

  data_type: 'varchar'
  default_value: 'http://'
  is_nullable: 1
  size: 255

=head2 url_extralink_documentation

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 1
  size: 255

=head2 license_array

  data_type: 'text'
  is_nullable: 1

=head2 devel_status_array

  data_type: 'text'
  is_nullable: 1

=head2 mailing_list_address

  data_type: 'varchar'
  default_value: '@'
  is_nullable: 1
  size: 255

=head2 mailing_list_virtual_host

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 1
  size: 255

=head2 mailing_list_format

  data_type: 'varchar'
  default_value: '%NAME'
  is_nullable: 0
  size: 255

=head2 forum_flags

  data_type: 'integer'
  default_value: 2
  is_nullable: 1

=head2 bugs_flags

  data_type: 'integer'
  default_value: 2
  is_nullable: 1

=head2 task_flags

  data_type: 'integer'
  default_value: 2
  is_nullable: 1

=head2 patch_flags

  data_type: 'integer'
  default_value: 2
  is_nullable: 1

=head2 cookbook_flags

  data_type: 'integer'
  default_value: 2
  is_nullable: 1

=head2 support_flags

  data_type: 'integer'
  default_value: 2
  is_nullable: 1

=head2 news_flags

  data_type: 'integer'
  default_value: 3
  is_nullable: 1

=head2 forum_rflags

  data_type: 'integer'
  default_value: 2
  is_nullable: 1

=head2 bugs_rflags

  data_type: 'integer'
  default_value: 2
  is_nullable: 1

=head2 task_rflags

  data_type: 'integer'
  default_value: 5
  is_nullable: 1

=head2 patch_rflags

  data_type: 'integer'
  default_value: 2
  is_nullable: 1

=head2 cookbook_rflags

  data_type: 'integer'
  default_value: 5
  is_nullable: 1

=head2 support_rflags

  data_type: 'integer'
  default_value: 2
  is_nullable: 1

=head2 news_rflags

  data_type: 'integer'
  default_value: 2
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "type_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "admin_email_adress",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "base_host",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "mailing_list_host",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "can_use_homepage",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "can_use_download",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "can_use_cvs",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "can_use_arch",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "can_use_svn",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "can_use_git",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "can_use_hg",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "can_use_bzr",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "can_use_license",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "can_use_devel_status",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "can_use_forum",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "can_use_mailing_list",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "can_use_patch",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "can_use_task",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "can_use_news",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "can_use_support",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "can_use_bug",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "is_menu_configurable_homepage",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_download",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_forum",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_support",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_mail",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_cvs",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_cvs_viewcvs",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_cvs_viewcvs_homepage",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_arch",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_arch_viewcvs",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_svn",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_svn_viewcvs",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_git",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_git_viewcvs",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_hg",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_hg_viewcvs",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_bzr",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_bzr_viewcvs",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_bugs",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_task",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_patch",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_menu_configurable_extralink_documentation",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_configurable_download_dir",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "homepage_scm",
  {
    data_type => "varchar",
    default_value => "cvs",
    is_nullable => 0,
    size => 25,
  },
  "dir_type_cvs",
  {
    data_type => "varchar",
    default_value => "basic",
    is_nullable => 0,
    size => 255,
  },
  "dir_type_arch",
  {
    data_type => "varchar",
    default_value => "basic",
    is_nullable => 0,
    size => 255,
  },
  "dir_type_svn",
  {
    data_type => "varchar",
    default_value => "basic",
    is_nullable => 0,
    size => 255,
  },
  "dir_type_git",
  {
    data_type => "varchar",
    default_value => "basic",
    is_nullable => 0,
    size => 255,
  },
  "dir_type_hg",
  {
    data_type => "varchar",
    default_value => "basic",
    is_nullable => 0,
    size => 255,
  },
  "dir_type_bzr",
  {
    data_type => "varchar",
    default_value => "basic",
    is_nullable => 0,
    size => 255,
  },
  "dir_type_homepage",
  {
    data_type => "varchar",
    default_value => "basic",
    is_nullable => 0,
    size => 255,
  },
  "dir_type_download",
  {
    data_type => "varchar",
    default_value => "basic",
    is_nullable => 0,
    size => 255,
  },
  "dir_homepage",
  { data_type => "varchar", default_value => "/", is_nullable => 1, size => 255 },
  "dir_cvs",
  { data_type => "varchar", default_value => "/", is_nullable => 1, size => 255 },
  "dir_arch",
  { data_type => "varchar", default_value => "/", is_nullable => 1, size => 255 },
  "dir_svn",
  { data_type => "varchar", default_value => "/", is_nullable => 1, size => 255 },
  "dir_git",
  { data_type => "varchar", default_value => "/", is_nullable => 0, size => 255 },
  "dir_hg",
  { data_type => "varchar", default_value => "/", is_nullable => 0, size => 255 },
  "dir_bzr",
  { data_type => "varchar", default_value => "/", is_nullable => 0, size => 255 },
  "dir_download",
  { data_type => "varchar", default_value => "/", is_nullable => 1, size => 255 },
  "url_homepage",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_download",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_cvs_viewcvs",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_arch_viewcvs",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_svn_viewcvs",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_git_viewcvs",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 0,
    size => 255,
  },
  "url_hg_viewcvs",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 0,
    size => 255,
  },
  "url_bzr_viewcvs",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 0,
    size => 255,
  },
  "url_cvs_viewcvs_homepage",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_mailing_list_listinfo",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_mailing_list_subscribe",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_mailing_list_unsubscribe",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_mailing_list_archives",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_mailing_list_archives_private",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_mailing_list_admin",
  {
    data_type => "varchar",
    default_value => "http://",
    is_nullable => 1,
    size => 255,
  },
  "url_extralink_documentation",
  { data_type => "varchar", default_value => "", is_nullable => 1, size => 255 },
  "license_array",
  { data_type => "text", is_nullable => 1 },
  "devel_status_array",
  { data_type => "text", is_nullable => 1 },
  "mailing_list_address",
  {
    data_type => "varchar",
    default_value => "\@",
    is_nullable => 1,
    size => 255,
  },
  "mailing_list_virtual_host",
  { data_type => "varchar", default_value => "", is_nullable => 1, size => 255 },
  "mailing_list_format",
  {
    data_type => "varchar",
    default_value => "%NAME",
    is_nullable => 0,
    size => 255,
  },
  "forum_flags",
  { data_type => "integer", default_value => 2, is_nullable => 1 },
  "bugs_flags",
  { data_type => "integer", default_value => 2, is_nullable => 1 },
  "task_flags",
  { data_type => "integer", default_value => 2, is_nullable => 1 },
  "patch_flags",
  { data_type => "integer", default_value => 2, is_nullable => 1 },
  "cookbook_flags",
  { data_type => "integer", default_value => 2, is_nullable => 1 },
  "support_flags",
  { data_type => "integer", default_value => 2, is_nullable => 1 },
  "news_flags",
  { data_type => "integer", default_value => 3, is_nullable => 1 },
  "forum_rflags",
  { data_type => "integer", default_value => 2, is_nullable => 1 },
  "bugs_rflags",
  { data_type => "integer", default_value => 2, is_nullable => 1 },
  "task_rflags",
  { data_type => "integer", default_value => 5, is_nullable => 1 },
  "patch_rflags",
  { data_type => "integer", default_value => 2, is_nullable => 1 },
  "cookbook_rflags",
  { data_type => "integer", default_value => 5, is_nullable => 1 },
  "support_rflags",
  { data_type => "integer", default_value => 2, is_nullable => 1 },
  "news_rflags",
  { data_type => "integer", default_value => 2, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</type_id>

=back

=cut

__PACKAGE__->set_primary_key("type_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gUFYcq9kb2JGtn/xVlaOWA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
