use utf8;
package Savane::Schema::Result::GroupsDefaultPermission;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::GroupsDefaultPermission

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<groups_default_permissions>

=cut

__PACKAGE__->table("groups_default_permissions");

=head1 ACCESSORS

=head2 groups_default_permissions_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 admin_flags

  data_type: 'char'
  is_nullable: 1
  size: 16

=head2 forum_flags

  data_type: 'integer'
  is_nullable: 1

=head2 bugs_flags

  data_type: 'integer'
  is_nullable: 1

=head2 cookbook_flags

  data_type: 'integer'
  is_nullable: 1

=head2 task_flags

  data_type: 'integer'
  is_nullable: 1

=head2 patch_flags

  data_type: 'integer'
  is_nullable: 1

=head2 support_flags

  data_type: 'integer'
  is_nullable: 1

=head2 news_flags

  data_type: 'integer'
  is_nullable: 1

=head2 forum_rflags

  data_type: 'integer'
  is_nullable: 1

=head2 bugs_rflags

  data_type: 'integer'
  is_nullable: 1

=head2 cookbook_rflags

  data_type: 'integer'
  is_nullable: 1

=head2 task_rflags

  data_type: 'integer'
  is_nullable: 1

=head2 patch_rflags

  data_type: 'integer'
  is_nullable: 1

=head2 support_rflags

  data_type: 'integer'
  is_nullable: 1

=head2 news_rflags

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "groups_default_permissions_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "admin_flags",
  { data_type => "char", is_nullable => 1, size => 16 },
  "forum_flags",
  { data_type => "integer", is_nullable => 1 },
  "bugs_flags",
  { data_type => "integer", is_nullable => 1 },
  "cookbook_flags",
  { data_type => "integer", is_nullable => 1 },
  "task_flags",
  { data_type => "integer", is_nullable => 1 },
  "patch_flags",
  { data_type => "integer", is_nullable => 1 },
  "support_flags",
  { data_type => "integer", is_nullable => 1 },
  "news_flags",
  { data_type => "integer", is_nullable => 1 },
  "forum_rflags",
  { data_type => "integer", is_nullable => 1 },
  "bugs_rflags",
  { data_type => "integer", is_nullable => 1 },
  "cookbook_rflags",
  { data_type => "integer", is_nullable => 1 },
  "task_rflags",
  { data_type => "integer", is_nullable => 1 },
  "patch_rflags",
  { data_type => "integer", is_nullable => 1 },
  "support_rflags",
  { data_type => "integer", is_nullable => 1 },
  "news_rflags",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</groups_default_permissions_id>

=back

=cut

__PACKAGE__->set_primary_key("groups_default_permissions_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:o5/zPWOxzPppOw/PW34tUQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
