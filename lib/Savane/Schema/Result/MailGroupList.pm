use utf8;
package Savane::Schema::Result::MailGroupList;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::MailGroupList

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<mail_group_list>

=cut

__PACKAGE__->table("mail_group_list");

=head1 ACCESSORS

=head2 group_list_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 list_name

  data_type: 'text'
  is_nullable: 1

=head2 is_public

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 password

  data_type: 'varchar'
  is_nullable: 1
  size: 16

=head2 list_admin

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 status

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 description

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "group_list_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "list_name",
  { data_type => "text", is_nullable => 1 },
  "is_public",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "password",
  { data_type => "varchar", is_nullable => 1, size => 16 },
  "list_admin",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "status",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "description",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</group_list_id>

=back

=cut

__PACKAGE__->set_primary_key("group_list_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:c6UMxoYuFrV52WrIz9sflw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
