use utf8;
package Savane::Schema::Result::NewsByte;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::NewsByte

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<news_bytes>

=cut

__PACKAGE__->table("news_bytes");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 submitted_by

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_approved

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 date

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 date_last_edit

  data_type: 'integer'
  is_nullable: 0

=head2 forum_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 summary

  data_type: 'text'
  is_nullable: 1

=head2 details

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "submitted_by",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_approved",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "date",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "date_last_edit",
  { data_type => "integer", is_nullable => 0 },
  "forum_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "summary",
  { data_type => "text", is_nullable => 1 },
  "details",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:DaF9CdfbzujIAW9+7eXRiQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
