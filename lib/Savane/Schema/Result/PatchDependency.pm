use utf8;
package Savane::Schema::Result::PatchDependency;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::PatchDependency

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<patch_dependencies>

=cut

__PACKAGE__->table("patch_dependencies");

=head1 ACCESSORS

=head2 item_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_dependent_on_item_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_dependent_on_item_id_artifact

  data_type: 'varchar'
  default_value: 0
  is_nullable: 0
  size: 255

=cut

__PACKAGE__->add_columns(
  "item_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_dependent_on_item_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_dependent_on_item_id_artifact",
  { data_type => "varchar", default_value => 0, is_nullable => 0, size => 255 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:mGIECiseyq+1SBzfMB2LGw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
