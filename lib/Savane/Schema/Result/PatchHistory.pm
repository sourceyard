use utf8;
package Savane::Schema::Result::PatchHistory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::PatchHistory

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<patch_history>

=cut

__PACKAGE__->table("patch_history");

=head1 ACCESSORS

=head2 bug_history_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 bug_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 field_name

  data_type: 'text'
  is_nullable: 0

=head2 old_value

  data_type: 'text'
  is_nullable: 1

=head2 new_value

  data_type: 'text'
  is_nullable: 0

=head2 mod_by

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 date

  data_type: 'integer'
  is_nullable: 1

=head2 spamscore

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 ip

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 type

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "bug_history_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "bug_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "field_name",
  { data_type => "text", is_nullable => 0 },
  "old_value",
  { data_type => "text", is_nullable => 1 },
  "new_value",
  { data_type => "text", is_nullable => 0 },
  "mod_by",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "date",
  { data_type => "integer", is_nullable => 1 },
  "spamscore",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "ip",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "type",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</bug_history_id>

=back

=cut

__PACKAGE__->set_primary_key("bug_history_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/re24kgfvtorfm+r5WhM3Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
