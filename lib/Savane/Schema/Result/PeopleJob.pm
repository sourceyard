use utf8;
package Savane::Schema::Result::PeopleJob;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::PeopleJob

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<people_job>

=cut

__PACKAGE__->table("people_job");

=head1 ACCESSORS

=head2 job_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 created_by

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 title

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 date

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 status_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 category_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "job_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "created_by",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "title",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "date",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "status_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "category_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</job_id>

=back

=cut

__PACKAGE__->set_primary_key("job_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:aAYkpe+IBJIDbfJ0AZlfEg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
