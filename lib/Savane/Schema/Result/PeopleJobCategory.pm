use utf8;
package Savane::Schema::Result::PeopleJobCategory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::PeopleJobCategory

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<people_job_category>

=cut

__PACKAGE__->table("people_job_category");

=head1 ACCESSORS

=head2 category_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "category_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</category_id>

=back

=cut

__PACKAGE__->set_primary_key("category_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:SflpDA24nc1PoGT1yqDp7g


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
