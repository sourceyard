use utf8;
package Savane::Schema::Result::PeopleJobInventory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::PeopleJobInventory

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<people_job_inventory>

=cut

__PACKAGE__->table("people_job_inventory");

=head1 ACCESSORS

=head2 job_inventory_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 job_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 skill_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 skill_level_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 skill_year_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "job_inventory_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "job_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "skill_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "skill_level_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "skill_year_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</job_inventory_id>

=back

=cut

__PACKAGE__->set_primary_key("job_inventory_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:PFVR7b0YC8pSCWRKmhXZ0A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
