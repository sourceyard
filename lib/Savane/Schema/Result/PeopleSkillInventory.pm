use utf8;
package Savane::Schema::Result::PeopleSkillInventory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::PeopleSkillInventory

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<people_skill_inventory>

=cut

__PACKAGE__->table("people_skill_inventory");

=head1 ACCESSORS

=head2 skill_inventory_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 skill_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 skill_level_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 skill_year_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "skill_inventory_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "skill_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "skill_level_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "skill_year_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</skill_inventory_id>

=back

=cut

__PACKAGE__->set_primary_key("skill_inventory_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:5V0/eSbirkrAlo5k+rOD4Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->has_one(skill => 'Savane::Schema::Result::PeopleSkill',
		     { 'foreign.skill_id' => 'self.skill_id' },
		     { cascade_delete => 0 });
__PACKAGE__->has_one(skill_level => 'Savane::Schema::Result::PeopleSkillLevel',
		     { 'foreign.skill_level_id' => 'self.skill_level_id' },
		     { cascade_delete => 0 });
__PACKAGE__->has_one(skill_experience => 'Savane::Schema::Result::PeopleSkillYear',
		     { 'foreign.skill_year_id' => 'self.skill_year_id' },
		     { cascade_delete => 0 });

__PACKAGE__->belongs_to('user' => 'Savane::Schema::Result::User',
			'user_id');

1;
