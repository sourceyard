use utf8;
package Savane::Schema::Result::PeopleSkillLevel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::PeopleSkillLevel

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<people_skill_level>

=cut

__PACKAGE__->table("people_skill_level");

=head1 ACCESSORS

=head2 skill_level_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "skill_level_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</skill_level_id>

=back

=cut

__PACKAGE__->set_primary_key("skill_level_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:g81raXclBSQjYD5ZuIaoxw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
# __PACKAGE__->belongs_to('skill' => 'Savane::Schema::Result::PeopleSkillInventory',
# 			'skill_level_id');

1;
