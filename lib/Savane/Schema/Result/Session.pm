use utf8;
package Savane::Schema::Result::Session;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::Session

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<session>

=cut

__PACKAGE__->table("session");

=head1 ACCESSORS

=head2 user_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 session_hash

  data_type: 'char'
  default_value: (empty string)
  is_nullable: 0
  size: 32

=head2 ip_addr

  data_type: 'char'
  default_value: (empty string)
  is_nullable: 0
  size: 15

=head2 time

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "user_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "session_hash",
  { data_type => "char", default_value => "", is_nullable => 0, size => 32 },
  "ip_addr",
  { data_type => "char", default_value => "", is_nullable => 0, size => 15 },
  "time",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</session_hash>

=back

=cut

__PACKAGE__->set_primary_key("session_hash");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:C2sq+ziPNCXbvJ2bj9I4tg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
