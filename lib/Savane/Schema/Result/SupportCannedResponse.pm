use utf8;
package Savane::Schema::Result::SupportCannedResponse;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::SupportCannedResponse

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<support_canned_responses>

=cut

__PACKAGE__->table("support_canned_responses");

=head1 ACCESSORS

=head2 bug_canned_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 title

  data_type: 'text'
  is_nullable: 1

=head2 body

  data_type: 'text'
  is_nullable: 1

=head2 order_id

  data_type: 'integer'
  default_value: 50
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "bug_canned_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "title",
  { data_type => "text", is_nullable => 1 },
  "body",
  { data_type => "text", is_nullable => 1 },
  "order_id",
  { data_type => "integer", default_value => 50, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</bug_canned_id>

=back

=cut

__PACKAGE__->set_primary_key("bug_canned_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:icD/djn3MGlGncTirS95kA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
