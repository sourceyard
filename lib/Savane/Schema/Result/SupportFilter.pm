use utf8;
package Savane::Schema::Result::SupportFilter;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::SupportFilter

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<support_filter>

=cut

__PACKAGE__->table("support_filter");

=head1 ACCESSORS

=head2 filter_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 sql_clause

  data_type: 'text'
  is_nullable: 0

=head2 is_active

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "filter_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "sql_clause",
  { data_type => "text", is_nullable => 0 },
  "is_active",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</filter_id>

=back

=cut

__PACKAGE__->set_primary_key("filter_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2iwGGbtaNxvQYrwRJ1YT1A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
