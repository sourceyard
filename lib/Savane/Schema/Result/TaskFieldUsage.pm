use utf8;
package Savane::Schema::Result::TaskFieldUsage;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::TaskFieldUsage

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<task_field_usage>

=cut

__PACKAGE__->table("task_field_usage");

=head1 ACCESSORS

=head2 bug_field_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 use_it

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 show_on_add

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 show_on_add_members

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 place

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 custom_label

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 custom_description

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 custom_display_size

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 custom_empty_ok

  data_type: 'integer'
  is_nullable: 1

=head2 custom_keep_history

  data_type: 'integer'
  is_nullable: 1

=head2 transition_default_auth

  data_type: 'char'
  default_value: 'A'
  is_nullable: 0
  size: 1

=cut

__PACKAGE__->add_columns(
  "bug_field_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "use_it",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "show_on_add",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "show_on_add_members",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "place",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "custom_label",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "custom_description",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "custom_display_size",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "custom_empty_ok",
  { data_type => "integer", is_nullable => 1 },
  "custom_keep_history",
  { data_type => "integer", is_nullable => 1 },
  "transition_default_auth",
  { data_type => "char", default_value => "A", is_nullable => 0, size => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:BHmp2KB637rDGYSOXVmMeA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
