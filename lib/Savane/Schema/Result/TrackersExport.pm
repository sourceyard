use utf8;
package Savane::Schema::Result::TrackersExport;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::TrackersExport

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<trackers_export>

=cut

__PACKAGE__->table("trackers_export");

=head1 ACCESSORS

=head2 export_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 task_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 artifact

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 16

=head2 unix_group_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 user_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 sql

  data_type: 'text'
  is_nullable: 0

=head2 status

  data_type: 'char'
  default_value: (empty string)
  is_nullable: 0
  size: 1

=head2 date

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 frequency_day

  data_type: 'integer'
  is_nullable: 1

=head2 frequency_hour

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "export_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "task_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "artifact",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 16 },
  "unix_group_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "user_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "sql",
  { data_type => "text", is_nullable => 0 },
  "status",
  { data_type => "char", default_value => "", is_nullable => 0, size => 1 },
  "date",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "frequency_day",
  { data_type => "integer", is_nullable => 1 },
  "frequency_hour",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</export_id>

=back

=cut

__PACKAGE__->set_primary_key("export_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:lwIJMYg8V16GXag6mHNp7Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
