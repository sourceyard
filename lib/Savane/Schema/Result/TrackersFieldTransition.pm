use utf8;
package Savane::Schema::Result::TrackersFieldTransition;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::TrackersFieldTransition

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<trackers_field_transition>

=cut

__PACKAGE__->table("trackers_field_transition");

=head1 ACCESSORS

=head2 transition_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 artifact

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 16

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 field_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 from_value_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 to_value_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 is_allowed

  data_type: 'char'
  default_value: 'Y'
  is_nullable: 1
  size: 1

=head2 notification_list

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "transition_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "artifact",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 16 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "field_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "from_value_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "to_value_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "is_allowed",
  { data_type => "char", default_value => "Y", is_nullable => 1, size => 1 },
  "notification_list",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</transition_id>

=back

=cut

__PACKAGE__->set_primary_key("transition_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:8RQVVlejIuiotUoTwgJKIg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
