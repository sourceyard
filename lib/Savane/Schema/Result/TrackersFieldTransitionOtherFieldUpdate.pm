use utf8;
package Savane::Schema::Result::TrackersFieldTransitionOtherFieldUpdate;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::TrackersFieldTransitionOtherFieldUpdate

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<trackers_field_transition_other_field_update>

=cut

__PACKAGE__->table("trackers_field_transition_other_field_update");

=head1 ACCESSORS

=head2 other_field_update_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 transition_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 update_field_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 255

=head2 update_value_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "other_field_update_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "transition_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "update_field_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 255 },
  "update_value_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</other_field_update_id>

=back

=cut

__PACKAGE__->set_primary_key("other_field_update_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:EqijaMBAVmouvqvQ0q64hg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
