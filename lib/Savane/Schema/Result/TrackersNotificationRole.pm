use utf8;
package Savane::Schema::Result::TrackersNotificationRole;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::TrackersNotificationRole

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<trackers_notification_role>

=cut

__PACKAGE__->table("trackers_notification_role");

=head1 ACCESSORS

=head2 role_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 role_label

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 short_description

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 description

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 rank

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "role_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "role_label",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "short_description",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "description",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "rank",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:tIJwRKgxeX28wZvLXfhjgg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
