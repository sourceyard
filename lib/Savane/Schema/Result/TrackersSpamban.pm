use utf8;
package Savane::Schema::Result::TrackersSpamban;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::TrackersSpamban

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<trackers_spamban>

=cut

__PACKAGE__->table("trackers_spamban");

=head1 ACCESSORS

=head2 ip

  data_type: 'char'
  default_value: (empty string)
  is_nullable: 0
  size: 15

=head2 date

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "ip",
  { data_type => "char", default_value => "", is_nullable => 0, size => 15 },
  "date",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:TE8h7vhKHruCxQETGZIj+Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
