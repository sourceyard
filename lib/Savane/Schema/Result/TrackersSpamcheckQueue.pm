use utf8;
package Savane::Schema::Result::TrackersSpamcheckQueue;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::TrackersSpamcheckQueue

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<trackers_spamcheck_queue>

=cut

__PACKAGE__->table("trackers_spamcheck_queue");

=head1 ACCESSORS

=head2 queue_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 artifact

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 16

=head2 item_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 comment_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 priority

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 date

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  default_value: current_timestamp
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "queue_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "artifact",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 16 },
  "item_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "comment_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "priority",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "date",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    default_value => \"current_timestamp",
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</queue_id>

=back

=cut

__PACKAGE__->set_primary_key("queue_id");

=head1 UNIQUE CONSTRAINTS

=head2 C<artifact>

=over 4

=item * L</artifact>

=item * L</item_id>

=item * L</comment_id>

=back

=cut

__PACKAGE__->add_unique_constraint("artifact", ["artifact", "item_id", "comment_id"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1xl0ZKURePNKZjhMGUFhOg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
