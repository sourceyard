use utf8;
package Savane::Schema::Result::TrackersSpamcheckQueueNotification;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::TrackersSpamcheckQueueNotification

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<trackers_spamcheck_queue_notification>

=cut

__PACKAGE__->table("trackers_spamcheck_queue_notification");

=head1 ACCESSORS

=head2 notification_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 artifact

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 16

=head2 item_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 comment_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 to_header

  data_type: 'text'
  is_nullable: 1

=head2 subject_header

  data_type: 'text'
  is_nullable: 1

=head2 other_headers

  data_type: 'text'
  is_nullable: 1

=head2 message

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "notification_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "artifact",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 16 },
  "item_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "comment_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "to_header",
  { data_type => "text", is_nullable => 1 },
  "subject_header",
  { data_type => "text", is_nullable => 1 },
  "other_headers",
  { data_type => "text", is_nullable => 1 },
  "message",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</notification_id>

=back

=cut

__PACKAGE__->set_primary_key("notification_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:m3hl3/GD+lbhVPs7lnFYpQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
