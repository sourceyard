use utf8;
package Savane::Schema::Result::TrackersSpamscore;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::TrackersSpamscore

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<trackers_spamscore>

=cut

__PACKAGE__->table("trackers_spamscore");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 score

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 affected_user_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 reporter_user_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 artifact

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 16

=head2 item_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 comment_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "score",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "affected_user_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "reporter_user_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "artifact",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 16 },
  "item_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "comment_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<artifact>

=over 4

=item * L</artifact>

=item * L</item_id>

=item * L</comment_id>

=item * L</affected_user_id>

=item * L</reporter_user_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "artifact",
  [
    "artifact",
    "item_id",
    "comment_id",
    "affected_user_id",
    "reporter_user_id",
  ],
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:7E0xdrYBYkpZ8J9Q7OnxYg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
