use utf8;
package Savane::Schema::Result::TrackersWatcher;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::TrackersWatcher

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<trackers_watcher>

=cut

__PACKAGE__->table("trackers_watcher");

=head1 ACCESSORS

=head2 user_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 watchee_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "user_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "watchee_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gkqGC0nhzUU5/OkN+cgFfw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
