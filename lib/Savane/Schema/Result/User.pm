use utf8;
package Savane::Schema::Result::User;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::User

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<user>

=cut

__PACKAGE__->table("user");

=head1 ACCESSORS

=head2 user_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 user_name

  data_type: 'varchar'
  is_nullable: 0
  size: 33

=head2 email

  data_type: 'text'
  is_nullable: 0

=head2 user_pw

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 realname

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 32

=head2 status

  data_type: 'varchar'
  default_value: 'A'
  is_nullable: 0
  size: 16

=head2 uidnumber

  data_type: 'integer'
  is_nullable: 1

=head2 spamscore

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 add_date

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 confirm_hash

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 authorized_keys

  data_type: 'text'
  is_nullable: 1

=head2 authorized_keys_count

  data_type: 'integer'
  is_nullable: 1

=head2 email_new

  data_type: 'text'
  is_nullable: 1

=head2 people_view_skills

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 people_resume

  data_type: 'text'
  is_nullable: 0

=head2 resume_spamscore

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 timezone

  data_type: 'varchar'
  default_value: 'GMT'
  is_nullable: 1
  size: 64

=head2 theme

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 1
  size: 15

=head2 email_hide

  data_type: 'varchar'
  default_value: 0
  is_nullable: 1
  size: 3

=head2 gpg_key

  data_type: 'text'
  is_nullable: 1

=head2 gpg_key_count

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "user_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "user_name",
  { data_type => "varchar", is_nullable => 0, size => 33 },
  "email",
  { data_type => "text", is_nullable => 0 },
  "user_pw",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "realname",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 32 },
  "status",
  { data_type => "varchar", default_value => "A", is_nullable => 0, size => 16 },
  "uidnumber",
  { data_type => "integer", is_nullable => 1 },
  "spamscore",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "add_date",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "confirm_hash",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "authorized_keys",
  { data_type => "text", is_nullable => 1 },
  "authorized_keys_count",
  { data_type => "integer", is_nullable => 1 },
  "email_new",
  { data_type => "text", is_nullable => 1 },
  "people_view_skills",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "people_resume",
  { data_type => "text", is_nullable => 0 },
  "resume_spamscore",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "timezone",
  {
    data_type => "varchar",
    default_value => "GMT",
    is_nullable => 1,
    size => 64,
  },
  "theme",
  { data_type => "varchar", default_value => "", is_nullable => 1, size => 15 },
  "email_hide",
  { data_type => "varchar", default_value => 0, is_nullable => 1, size => 3 },
  "gpg_key",
  { data_type => "text", is_nullable => 1 },
  "gpg_key_count",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</user_id>

=back

=cut

__PACKAGE__->set_primary_key("user_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Z+EFm6oXpDsAy4/8brRRJg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->has_many(
    skills => 'Savane::Schema::Result::PeopleSkillInventory',
    'user_id'
);



1;
