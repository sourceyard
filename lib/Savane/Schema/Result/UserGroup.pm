use utf8;
package Savane::Schema::Result::UserGroup;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::UserGroup

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<user_group>

=cut

__PACKAGE__->table("user_group");

=head1 ACCESSORS

=head2 user_group_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 admin_flags

  data_type: 'char'
  is_nullable: 1
  size: 16

=head2 cache_uidnumber

  data_type: 'integer'
  is_nullable: 1

=head2 cache_gidnumber

  data_type: 'integer'
  is_nullable: 1

=head2 cache_user_name

  data_type: 'varchar'
  is_nullable: 1
  size: 33

=head2 onduty

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 forum_flags

  data_type: 'integer'
  is_nullable: 1

=head2 bugs_flags

  data_type: 'integer'
  is_nullable: 1

=head2 task_flags

  data_type: 'integer'
  is_nullable: 1

=head2 patch_flags

  data_type: 'integer'
  is_nullable: 1

=head2 support_flags

  data_type: 'integer'
  is_nullable: 1

=head2 cookbook_flags

  data_type: 'integer'
  is_nullable: 1

=head2 news_flags

  data_type: 'integer'
  is_nullable: 1

=head2 privacy_flags

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "user_group_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "group_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "admin_flags",
  { data_type => "char", is_nullable => 1, size => 16 },
  "cache_uidnumber",
  { data_type => "integer", is_nullable => 1 },
  "cache_gidnumber",
  { data_type => "integer", is_nullable => 1 },
  "cache_user_name",
  { data_type => "varchar", is_nullable => 1, size => 33 },
  "onduty",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "forum_flags",
  { data_type => "integer", is_nullable => 1 },
  "bugs_flags",
  { data_type => "integer", is_nullable => 1 },
  "task_flags",
  { data_type => "integer", is_nullable => 1 },
  "patch_flags",
  { data_type => "integer", is_nullable => 1 },
  "support_flags",
  { data_type => "integer", is_nullable => 1 },
  "cookbook_flags",
  { data_type => "integer", is_nullable => 1 },
  "news_flags",
  { data_type => "integer", is_nullable => 1 },
  "privacy_flags",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</user_group_id>

=back

=cut

__PACKAGE__->set_primary_key("user_group_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:74Ra/G9N67IG3mbyt8tEfQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
