use utf8;
package Savane::Schema::Result::UserVote;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Savane::Schema::Result::UserVote

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<user_votes>

=cut

__PACKAGE__->table("user_votes");

=head1 ACCESSORS

=head2 vote_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 tracker

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 16

=head2 item_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 howmuch

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "vote_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "tracker",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 16 },
  "item_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "howmuch",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</vote_id>

=back

=cut

__PACKAGE__->set_primary_key("vote_id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-19 12:15:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:M8qnZHEzp8OEnADhtk/87A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
