package Sourceyard;
use Mojo::Base 'Mojolicious';
use FindBin;
use Sourceyard::Config;
use Sourceyard::Schema;
use Sourceyard::Controller;
use Sourceyard::Log::Syslog;
use Carp;

sub config {
    my $self = shift;
    return $self->{_config};
}

# This method will run once at server start
sub startup {
  my $self = shift;

  my $config = $self->{_config} = new Sourceyard::Config;

  if (my $sec = $config->get(qw(session secret))) {
      $self->app->secrets($sec);
  }
  $self->sessions->default_expiration($config->get(qw(sessions ttl)));
  
  if ($config->get(qw(log syslog enable))) {
      $self->app->log(new Sourceyard::Log::Syslog(
			  facility => $config->get(qw(log syslog facility))
		      ));
  }
  $self->app->log->level($config->get(qw(log level)));
  $self->app->log->short($config->get(qw(log short)));
  
  $self->helper(config => sub { return $config });
  $self->helper(log => sub { $self->app->log });

  my $schema = Sourceyard::Schema->connect($config);
  $self->helper(db => sub { return $schema; });

  $self->helper(top_menu_item => \&_top_menu_item);
  $self->helper(alt_tag => \&_alt_tag);
  $self->helper(opt_input => \&_opt_input);
  $self->helper(opt_buttons => \&_opt_buttons);
  $self->helper(theme_list => \&_theme_list);
  $self->helper(skill_list => \&_skill_list);
  $self->helper(skill_level_list => \&_skill_level_list);
  $self->helper(skill_experience_list => \&_skill_experience_list);
  $self->helper(input_error => \&_tag_input_error);
  
  $self->plugin("ACL", $config);

  if ($config->get(qw(register enable))
      && $config->get(qw(register captcha enable))) {
      my %newargs;
      my $st = $config->getnode(qw(register captcha))->subtree;
      @newargs{keys %{$st}} = map { $_->value } values %{$st};
      $self->plugin('captcha', {
		session_name => 'captcha_string',
		out => { force => 'jpeg' },
#		particle => [2000, 5],
		create => [qw(ttf blank)],
		new => {
		    rnd_data => [0...9, 'A'...'Z', 'a'...'z'],
		    %newargs
		}
         });
  }
  
  # FIXME: Documentation browser under "/perldoc"
  $self->plugin('PODRenderer');

  $self->app->defaults(user => undef);
  $self->app->defaults(return_url => undef);
  $self->app->defaults('sourceyard.alt' => 0);
  $self->app->defaults(theme => $config->get(qw(core theme)));

  unshift @{$self->app->renderer->paths},  $config->public_file('templates');
  
  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('main#index');

  $r->get('/account/:action' => [action => [qw(login logout lostpw)]])
      ->to(controller => 'account');
  $r->post('/account/login')->name('login')->to('account#user_login');
  $r->post('/account/lostpw')->to('account#lostpw');
  $r->get('/account/lostpw/:confirmhash')->to('account#recoverpw');
  
  if ($config->get(qw(register enable))) {
      $r->any(['GET', 'POST'] => '/account/register')->to('account#register');
  } else {
      $r->get('/account/register')->to('account#registerhint');
  }
  $r->get('/account/confirm/:confirmhash')->to('account#confirm');
  $r->post('/account/changepw')->to('account#changepw');
  
  my $auth = $r->under('/user' => sub {
      my $c = Sourceyard::Controller->clone(shift);
      unless ($c->stash('user')) {
	  $c->render('/account/login', return_url => $c->req->url);
	  return undef
      }
      return 1;
  });
  $auth->any(['GET', 'POST'] => '/:action' =>
	     [action => [ qw(admin ssh_keys gpg_keys resume) ]])
         ->to(controller => 'user');

  $r->get('/viewtemplate/:template')->to('account#viewtemplate');
}

sub _top_menu_item {
    my ($c, $tgt, $title, $content) = @_;

    my $path = $c->req->url->path;
    $path =~ s{^[^/]}{/};
    $path =~ s{/$}{};

    $tgt =~ s{^[^/]}{/};    
    $tgt =~ s{/$}{};

    my $class = $tgt eq $path ? 'tabselect' : 'tabs';
    
    return $c->tag('li', class => "topmenuitemmainitem",
		   $c->tag('a', href => $tgt, class => $class,
			   title => $title, $content));
}

sub _alt_tag {
    my ($c, $tag) = (shift, shift);

    my $last;
    if (ref $_[-1] eq 'CODE') {
	$last = pop;
    }
    if (@_ % 2 == 0) {
	local %_ = @_;
	my $n = delete $_{init} || $c->stash('sourceyard.alt');
	if ($n) {
	    my @classes = split(/\s+/, $_{class});
	    $_{class} = join(' ', shift(@classes).'alt', @classes);
	}
	$c->stash('sourceyard.alt' => !$n);
	@_ = %_;
    }
    push @_, $last if defined($last);
    return $c->tag($tag, @_);
}

# opt_input PARAM, NAME => VALUE, ATTR....
sub _opt_input {
    my ($c, $param, $name) = (shift, shift, shift);

    if ($c->param("change_$param")) {
	return $c->text_field($name, @_);
    } else {
	my @args;
	if (@_ % 2) {
	    push @args, shift;
	}
	%_ = @_;
	delete $_{tabindex};
	push @args, %_;
	return $c->tag('strong', @args);
    }
}

# opt_buttons SUFFIX
sub _opt_buttons {
    my ($c, $suffix, %btnp) = @_;
    my $str;

    my $srx = qr($suffix);
    my $disabled = (grep { /^change_(?!$srx)/ } @{$c->req->params->names})
	           ? 'disabled' : '';
    return Mojo::ByteStream->new(
	$c->render_to_string(
	    'opt/'.
	    ($c->param("change_$suffix") ? 'button.true' : 'button.false'),
	    suffix => $suffix,
	    disabled => $disabled,
	    %btnp
	));
}

sub _theme_list {
    my ($c, $cur) = @_;
    my $pubdir = ${$c->app->static->paths}[0];
    my $pfx = qr{$pubdir/css/};
    my @ret = sort
	map {
	    (m{^$pfx(.*)\.css$} && -d "$pubdir/images/$1.theme") ? $1 : ()
        } glob "$pubdir/css/*.css";
    if ($cur) {
	return map {
	    my @elt = ( $_ => $_ );
	    push @elt, selected => 'selected' if ($_ eq $cur);
	    \@elt } @ret;
    }
    return @ret;
}

sub _skill_list {
    my $c = shift;
    local %_ = @_;
    my $cond;
    if ($_{except}) {
	my @a = grep { defined($_) } @{$_{except}};
	if (@a) {
	    $cond = { skill_id => { -not_in => \@a } };
	}
    }
    return map {
	[ $_->name => $_->skill_id ]
    } $c->db->resultset('Skill')
	->search($cond, { order_by => 'name' })->all;
}

sub _skill_level_list {
    my ($c, $cur) = @_;
    if (defined($cur) && ref($cur) && $cur->can('skill_level_id')) {
	$cur = $cur->skill_level_id;
    }
    return map {
	my @elt = ( $_->name => $_->skill_level_id );
	push @elt, selected => 'selected'
	    if defined($cur) && ($_->skill_level_id == $cur);
	\@elt
    } $c->db->resultset('SkillLevel')
	->search(undef, { order_by => 'skill_level_id' })->all;
}

sub _skill_experience_list {
    my ($c, $cur) = @_;
    if (defined($cur) && ref($cur) && $cur->can('skill_experience_id')) {
	$cur = $cur->skill_experience_id;
    }
    return map {
	my @elt = ( $_->name => $_->skill_experience_id );
	push @elt, selected => 'selected'
	    if defined($cur) && ($_->skill_experience_id == $cur);
	\@elt
    } $c->db->resultset('SkillExperience')
	->search(undef, { order_by => 'skill_experience_id' })->all;
}

sub _tag_input_error {
    my ($c, $name) = @_;
    if (my $err = $c->stash($name.'_error')) {
	return $c->tag('span', class => "error notice", $err);
    }
}


1;

    
    
