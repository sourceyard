package Sourceyard::Config;
use strict;
use warnings;
use Carp;
use FindBin;
use Cwd 'abs_path';
use Sys::Hostname;

use parent 'App::Sourceyard::Config::Cached';

my %syntax = (
    core => {
	section => {
	    sitename => { default => 'Sourceyard' },
	    domain => {	default => hostname() },
	    acl => 1, # ACL file name
	    theme => { default => 'Savannah' }, # Backward-compatibility
	}
    },
    session => {
	section => {
	    secret => { array => 1 },
	    ttl => { default => 86400,
		     check => \&_check_interval }
	}
    },
    database => {
	section => {
	    host => { default => "localhost" },
	    name => { default => "sourceyard" },
	    user => 1,
	    password => 1,
	    params => 1,
	    'config-file' => 1
	}
    },
    savane => {
	section => {
	    database => {
		section => {
		    host => { default => "localhost" },
		    name => { default => "savane" },
		    user => 1,
		    password => 1,
		    params => 1,
		    'config-file' => 1
		}
	    }
	}
    },
    auth => {
	section => {
	    upgrade_hash => {
		check => \&_check_bool
	    }
	}
    },
    register => {
	section => {
	    enable => {
		check => \&_check_bool,
		default => 1
	    },
	    captcha => {
		section => {
		    enable => {
			check => \&_check_bool,
			default => sub {
			    shift->get(qw(register enable))
			}
		    },
		    width    => { default => 280 },
		    height   => { default => 68 },
		    lines    => { default => 10 },
		    thickness => { default => 1 },
		    rndmax => { default => 6 },
		    scramble => { default => 1 },
		    send_ctobg => { default => 1 },
		    font => {
			default => sub {
			    shift->public_file('fonts/DejaVuSerif-Bold.ttf')
			},
			check => \&_check_public_file
		    },
		    ptsize => { default => 20 }
		}
	    },
	    'confirmation-ttl' => { default => 2*86400,
				    check => \&_check_interval }
        }
    },
    mail => {
	section => {
	    mail_domain => {
		default => hostname()
	    },
	    admin_address => {
		default => 'root@'.hostname()
	    },
	    sender_address => {
		default => $ENV{LOGNAME}.'@'.hostname()
	    }
	}
    },
    log => {
	section => {
	    level => { default => 'debug',
		       check => sub {
			   my ($self, $vref, $prev, $loc) = @_;
			   return 1
			       if $$vref =~ m{^(debug|error|fatal|info|warn)$};
			   $self->error("unrecognized log level",
					locus => $loc);
			   return 0;
		       }
	    },
	    short => { default => 0, check => \&_check_bool },
	    syslog => {
		section => {
		    enable => { default => 0, check => \&_check_bool },
		    facility => {
			default => 'user',
			check => sub {
			    my ($self, $vref, $prev, $loc) = @_;
			    return 1
				if $$vref =~ m{^(auth(priv)?|(cr|daem)on|ftp|kern|l(ocal[0-7]|pr)|mail|news|u(ser|ucp))$};
			    $self->error("unrecognized facility",
					 locus => $loc);
			    return 0;
			}
		    }
		}
	    }
	}
    }
);

sub new {
    my $class = shift;
    my $basedir = abs_path("$FindBin::Bin/..");
    my $self = $class->SUPER::new("$basedir/sourceyard.conf",
				  parameters => \%syntax,
				  cache => 1,
				  @_);
    $self->parse() or croak "configuration failed";
    $self->{_public_dir} = "$basedir/public";
    return $self;
}

sub public_dir {
    my ($self) = @_;
    return $self->{_public_dir};
}

sub public_file {
    my ($self, $file) = @_;
    return $self->public_dir unless defined($file);
    return $file if $file =~ m{^/};
    return $self->public_dir . '/' . $file
}

# Syntax checkers

sub _check_bool {
    my ($self, $vref, $prev, $loc) = @_;
    my %btab = (
	1 => 1,
	0 => 0,
	'yes'  => 1,
	'no'   => 0,
	'true' => 1,
	'false' => 0,
	'on'  => 1,
	'off' => 0,
	't'   => 1,
	'nil' => 0
    );
    my $v = lc $$vref;
    unless (exists($btab{$v})) {
	$self->error("not a boolean: $$vref", locus => $loc);
	return 0;
    }
    $$vref = $btab{$v};
    return 1;
}

sub _check_dir {
    my ($self, $vref, $prev, $loc) = @_;
    unless (-e $$vref) {
	$self->error("directory $$vref does not exist", locus => $loc);
	return 0;
    }
    unless (-d $$vref) {
	$self->error("$$vref is not a directory", locus => $loc);
	return 0;
    }
    return 1;
}

sub _check_public_file {
    my ($self, $vref, $prev, $loc) = @_;
    my $file = $self->public_file($$vref);
    unless (-e $file) {
	$self->error("file $file does not exist", locus => $loc);
	return 0;
    }
    $$vref = $file;
    return 1;
}
	
sub _check_int {
    my ($self, $vref, $prev, $loc) = @_;
    unless ($$vref =~ /^\d+$/) {
	$self->error("not a valid number: $$vref", locus => $loc);
	return 0;
    }
    return 1;
}

sub _check_unix_user {
    my ($self, $vref, $prev, $loc) = @_;
    unless (defined(getpwnam($$vref))) {
	$self->error("no such user: $$vref", locus => $loc);
	return 0;
    }
    return 1;
}

sub _check_unix_group {
    my ($self, $vref, $prev, $loc) = @_;
    unless (defined(getgrnam($$vref))) {
	$self->error("no such group: $$vref", locus => $loc);
	return 0;
    }
    return 1;
}

sub _check_shell {
    my ($self, $vref, $prev, $loc) = @_;
    my $shell = $$vref;
    unless (-e $shell) {
	$self->error("file $shell does not exist", locus => $loc);
	return 0;
    }
    unless (-x $shell) {
	$self->error("file $shell is not executable", locus => $loc);
	return 0;
    }
    
    return 1;
}

sub _check_size {
    my ($self, $vref, $prev, $loc) = @_;

    unless ($$vref =~ /^\d+([kKmMgG][bB]?)?$/) {
	$self->error("not a valid size: $$vref", locus => $loc);
	return 0;
    }
    return undef;
}

sub __check_enum {
    my ($self, $val, $loc) = (shift, shift, shift);
    unless (grep { $_ eq $val } @_) {
	$self->error("valid values are: " . join(', ', @_), locus => $loc);
	return 0;
    }
    return 1;
}

sub _check_interval {
    my ($self, $vref, $prev, $loc) = @_;
    require App::Sourceyard::ParseInterval;
    my $err;
    if (my $val = parse_interval($$vref, \$err)) {
	$$vref = $val;
	return 1;
    } else {
	$self->error("bad time interval: $err", locus => $loc);
	return 0;
    }
}

1;
