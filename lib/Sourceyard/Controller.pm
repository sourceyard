package Sourceyard::Controller;

use strict;
use warnings;
use Mojo::Base 'Mojolicious::Controller';

use Sourceyard::User;
use Sourceyard::Statistics;
use DateTime;

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);
    $self->set_user;
    return $self;
}

sub clone {
    my ($class,$obj) = @_;
    my $self = bless $obj, $class;
    $self->set_user;
    return $self;
}

sub session {
    my $self = shift;
    if (@_) {
	if (@_ > 1) {
	    $self->session({@_});
	} elsif (ref($_[0])) {
	    if (exists($_[0]->{expires}) && $_[0]->{expires} < time) {
		my $key = $self->SUPER::session('session_key');
		my $sess = $self->db->resultset('Session')
		                ->single({ session_key => $key });
		if ($sess) {
		    $self->log->info("$self: deleting session $key");
		    $sess->delete;
		} else {
		    $self->log->error("$self: attemping to delete non-existing session $key");
		}
		$_[0]->{session_key} = undef;
	    }
	}
    }

    $self->SUPER::session(@_);
}

sub set_user {
    my ($self, $user) = @_;
    if (defined($user)) {
	$user->create_session;
    } elsif (my $key = $self->session('session_key')) {
	$user = $self->user_for_session($key);
	unless ($user) {
	    $self->session(expires => 1);
	}
    }

    my $theme;
    
    if ($user) {
	$self->stash(user => $user);
	$theme = ucfirst($user->theme);
    }
    
    unless (defined($theme)
	    && -f ${$self->app->static->paths}[0] . '/css/' . $theme . '.css') {
	$theme = $self->config->get(qw(core theme));
    }
    $self->stash(theme => $theme);
}

sub statistics {
    my $self = shift;
    return $self->{_sy_stat} ||= new Sourceyard::Statistics($self);
}

sub user_for_hash {
    my ($self, $hash) = @_;
    my $dt = DateTime->from_epoch(
	       epoch =>
	        time - $self->config->get(qw(register confirmation-ttl)));

    # DBI 1.622 claims that:
    #   DBIx::Class::Storage::DBI::_gen_sql_bind(): DateTime objects passed to
    #   search() are not supported properly (InflateColumn::DateTime formats
    #   and settings are not respected.) See ".. format a DateTime object for
    #   searching?" in DBIx::Class::Manual::FAQ.
    # with the cited article proposing a Ruby Goldbergs work out. However,
    # my tests have shown that it does work properly for the conditiion below.
    # I preferred to silent the warning. If, however, it is discovered that the
    # code below malfunctions, then replace the $dt below with:
    #    $self->db->storage->datetime_parser->format_datetime($dt);
    $ENV{DBIC_DT_SEARCH_OK} = 1;
    my $ch = $self->db->resultset('ConfirmHash')
	          ->single({ confirm_hash => $hash,
			     date => { '>=', $dt } });
    $ENV{DBIC_DT_SEARCH_OK} = 0;
    return $ch->user_id
	if ($ch);
}

sub user_for_session {
    my ($self, $key) = @_;
    my $dt = DateTime->from_epoch(
	epoch => time - $self->config->get(qw(session ttl)));
    
    my $s = $self->db->resultset('Session')
	         ->single({ session_key => $key });
    if ($s) {
	if ($s->expires_on->epoch > time) {
	    return $s->user_id;
	} else {
	    $s->session(expires => 1);
	}
    }
    return undef;
}

sub _str {
    my $self = shift;
    my @p;
    push @p, $self->remote_addr;
    if (my $user = $self->stash('user')) {
	push @p, $user->user_name;
    } else {
	push @p, 'anonymous';
    }
    return '['.join(' ', @p).']'
}

use overload '""' => sub { shift->_str };

1;
