package Sourceyard::Controller::Account;
use Mojo::Base 'Sourceyard::Controller';
use Sourceyard::User;
use Sourceyard::User::Template;
use Sourceyard::Mailer;

sub login {
    my $self = shift;
    if ($self->stash('user')) {
	$self->res->code(302);
	$self->redirect_to('/');
	return;
    }
    $self->render;
}

sub logout {
    my $self = shift;
    $self->session(expires => 1);
    $self->res->code(302);
    $self->redirect_to('/');
}

sub user_login {
    my $self = shift;

    my $username = $self->param('username');
    if (!$username) {
	$self->render('account/login', error_msg => 'No user name');
	return;
    }
    my $password = $self->param('password');
    my $remember = $self->param('cookie_for_a_year');
    my $return_url = $self->param('return_url') || '/';
    
    my $user = new Sourceyard::User($self, $username);

    if ($user && $user->user_pw->check($password)) {
	if ($user->user_pw->upgradable) {
	    $self->app->log->info($user->user_name . ': password needs upgrading');
	    if ($self->config->get(qw(auth upgrade_hash))) {
		$self->app->log->info($user->user_name . ': password upgraded');
		$user->user_pw(new App::Sourceyard::Password($password));
		$user->update;
	    }
	}
	$self->log->info("$self: ". $user->user_name." logged in");
	$self->set_user($user);
	$self->redirect_to($return_url);
    } else {
	$self->log->info("$self: ". $user->user_name.": login failed");
	$self->render('account/login',
		      error_msg => 'Invalid user name or password');
    }
}

sub register {
    my $self = shift;
    my %err;
    if ($self->req->method eq 'POST' && !$self->param('change_captcha')) {
	if ($self->config->get(qw(register captcha enable))) {
	    my $captcha = $self->param('captcha');
	    unless ($self->validate_captcha($captcha, 1)) {
		$self->render(undef,
			      captcha_error => 'Verification failed');
		return;
	    }
	}
	if ($self->param('password1') ne $self->param('password2')) {
	    $self->param(password1 => undef);
	    $self->param(password2 => undef);
	    $self->render(undef,
			  password_error => "Passwords don't match");
	    return;
	}

	my $t = new Sourceyard::User::Template(
            $self,
	    username => $self->param('username'),
	    realname => $self->param('realname'),
	    password => $self->param('password1'),
	    email => $self->param('email')
	    );

	if ($t->validate) {
	    my $user = new Sourceyard::User($self, $t);
	    Sourceyard::Mailer
		->new($self,
		      to => $user->email,
		      subject => "Registration of " . $user->user_name . " at " . $self->config->get(qw(core domain)))
		->send('mail/account/register',
		       new_user => $user,
		       confirm_address =>
		           $self->url_for('/account/confirm/'
					  . $user->confirm_hash->confirm_hash)
		                                 ->to_abs);
	    $self->log->info("$self: new user registered: ".
			     $user->user_name.
			     ' ('. $user->real_name.')'.
			     ' <'. $user->email .'>');
	    $self->render('account/registered', user_name => $user->user_name);
	    return;
	} else {
	    my $summary = $t->error;
	    @err{map { $_ . '_error' } keys %$summary} = values %$summary;
	}
	$self->param(captcha => undef);
    }    
    $self->render(undef, %err);
}

sub confirm {
    my $self = shift;
    my $hash = $self->stash('confirmhash');
    my $user = $self->user_for_hash($hash);
    if ($user && $user->status->is_pending) {
	$user->update({  status => $user->status->set_active });
	$user->confirm_hash->delete;
	$self->log->info("$self: ". $user->user_name.": account confirmed");
	$self->render('account/confirmed', user_name => $user->user_name);
	return;
    }
    $self->render('account/badconfirm');
}    

sub lostpw {
    my $self = shift;
    if ($self->req->method eq 'POST') {
	my $username = $self->param('username');
	if (!$username) {
	    $self->render(undef, error_msg => 'No user name');
	    return;
	}
	my $user = new Sourceyard::User($self, $username);
	if ($user) {
	    my $hash = $user->confirm_hash(1)->confirm_hash;
	    Sourceyard::Mailer
		->new($self,
		      to => $user->email,
		      subject => "Password recovery for " . $user->user_name . " at " . $self->config->get(qw(core domain)))
		->send('mail/account/recover',
		       new_user => $user,
		       confirm_address =>
		           $self->url_for('/account/lostpw/'.$hash) ->to_abs);
	    $self->log->info("$self: requested password recovery for " . $user->user_name);
	} else {
	    $self->render(undef, error_msg => 'No such user');
	}
    }
    return $self->render;
}

sub recoverpw {
    my $self = shift;
    my $hash = $self->stash('confirmhash');
    my $user = $self->user_for_hash($hash);
    if ($user && $user->status->is_active) {
	$self->render('account/changepw',
		      user_name => $user->user_name,
		      confirm_hash => $hash);
	return;
    }
    $self->render('account/badconfirm');
}

sub changepw {
    my $self = shift;
    my $hash = $self->param('confirm_hash');
    if (!$hash) {
	$self->render(undef, error_msg => 'Bad usage');
    }

    my %err;
    
    my $password = $self->param('password1');
    my $t = new Sourceyard::User::Template($self, password => $password);
    if ($password ne $self->param('password2')) {
	$err{password2} = 'Passwords differ';
    }

    if ($t->validate_password) {
	if (keys(%err) == 0) {
	    my $user = $self->user_for_hash($hash);
	    $user->user_pw($t->password);
	    $user->update;
	    $user->confirm_hash->delete;
	    $self->log->info("$self: password changed for " . $user->user_name);
	    $self->render('account/recovered');
	    return;
	}
    } else {
	$err{password1} = $t->error('password');
    }

    @err{map { $_ . '_error' } keys %err} = values %err;

    $self->render(undef, confirm_hash => $hash, %err);
}

sub viewtemplate {
    my $self = shift;
    Sourceyard::Mailer
	->new($self,
	      to => 'gray@gnu.org',
	      subject => "Registration of foobar at ")
		->send('/mail/account/register');		    
    $self->render('account/'.$self->stash('template'));
}

1;
