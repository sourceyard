package Sourceyard::Controller::Main;
use Mojo::Base 'Sourceyard::Controller';

# This action will render a template
sub index {
  my $self = shift;
  $self->render(msg => 'Welcome',
		statistics => $self->statistics);
}

sub login {
    my $self = shift;
    $self->render;
}

1;
