package Sourceyard::Controller::User;
use Mojo::Base 'Sourceyard::Controller';
use Sourceyard::User;
use Sourceyard::User::Template;
use Try::Tiny;

sub _update_signif {
    my $self = shift;
    my $user = $self->stash('user');
    my $name = $user->validate_real_name($self->param('realname'));
    if ($name) {
	if ($name ne $user->real_name) {
	    $user->real_name($name);
	    $self->{_sy_update} = 1;
	}
    } else {
	$self->stash('error_msg', 'Suspicious real name');
    }
}

sub _update_mail {
    my $self = shift;
    my $user = $self->stash('user');
    my $t = new Sourceyard::User::Template($self,
					   email => $self->param('email'));
    if ($t->validate_email) {
	if ($t->email ne $user->email) {
	    $user->email($t->email);
	    $self->{_sy_update} = 1;
	}
    } else {
	$self->stash('error_msg', "Invalid email: ".$t->error('email'));
    }
}    

sub _update_2 {
    my $self = shift;
    my $user = $self->stash('user');
    my $v;
    if (($v = $self->param('timezone')) ne $user->timezone) {
	$user->timezone($v);
	$self->{_sy_update} = 1;
    }
    if (($v = $self->param('theme')) ne $user->theme) {
	$user->theme($v);
	$self->stash('theme', $v);
	$self->{_sy_update} = 1;
    }
}    

sub _update_pass {
    my $self = shift;
    my $pass = $self->param('password1');
    if ($pass ne $self->param('password2')) {
	$self->stash('error_msg', "Passwords don't match");
	return;
    }
    my $user = $self->stash('user');
    my $t = new Sourceyard::User::Template($self,
					   password => $pass);
    if ($t->validate_password) {
	$user->user_pw($t->password);
	$self->{_sy_update} = 1;
    } else {
	$self->stash('error_msg', "Bad password: ".$t->error('password'));
    } 
    return $pass;
}

sub admin {
    my $self = shift;
    if ($self->req->method eq 'POST') {
	$self->{_sy_update} = 0;
	foreach my $act (qw(signif mail 2 pass)) {
	    if ($self->param('update_'.$act)) {
		my $meth = "_update_$act";
		$self->$meth();
	    }
	}
	if ($self->{_sy_update}) {
	    my $user = $self->stash('user');
	    $user->update;
	}
	if ($self->param('delete_hash')) {
	    my $user = $self->stash('user');
	    $user->confirm_hash->delete;
	    # Re-read user from the storage
	    $user->discard_changes;
	}
    }
    $self->render;
}

sub ssh_keys {
    my $self = shift;
    if ($self->req->method eq 'POST') {
	my $user = $self->stash('user');
	$self->{_sy_update} = 0;
	my $i;
	foreach my $k ($user->authorized_keys->all) {
	    $i++;
	    if ($self->param("del_$i")) {
		$self->db->resultset('Authorized_Keys')
		    ->find({ key_id => $k->key_id })->delete;
		$self->{_sy_update} = 1;
		last;
	    }
	    my $s = $self->param("key_$i"); # FIXME: Normalize
	    if ($s ne $k->ssh_key) {
		if ($s = $user->validate_ssh_key($s)) {
		    $self->db->resultset('Authorized_Keys')
			->find({ key_id => $k->key_id })->ssh_key($s);
		    $self->{_sy_update} = 1;
		} else {
		    $self->stash('error_msg', "Invalid SSH key #$i");
		}
		last;
	    }
	}

	if (my $s = $self->param("key_new")) {
	    $i++;
	    if ($s = $user->validate_ssh_key($s)) {
		$self->db->resultset('Authorized_Keys')
		    ->create({
			user_id => $user->user_id,
			ssh_key => $s
			     });
		$self->{_sy_update} = 1;
		$self->param(key_new => undef);
	    } else {
		$self->stash('error_msg', "Invalid SSH key #$i");
	    }
	}
    }
    $self->render;
}

sub gpg_keys {
    my $self = shift;
    if ($self->req->method eq 'POST') {
	my $user = $self->stash('user');
	$self->{_sy_update} = 0;
	my $i;
	foreach my $k ($user->gpg_keys->all) {
	    $i++;
	    if ($self->param("del_$i")) {
		$self->db->resultset('GPG_Keys')
		    ->find({ key_id => $k->key_id })->delete;
		$self->{_sy_update} = 1;
		last;
	    }
	}

	if (my $upload = $self->param('ascfile')) {
	    if ($self->req->is_limit_exceeded) {
		$self->stash('error_msg', 'File is too big');
	    } else {
		my $mesg;
		if (my $s = $user->validate_gpg_key($upload->slurp, \$mesg)) {
		    foreach my $key ($s->pubkeys) {
			$self->db->resultset('GPG_Keys')
			    ->create({
				user_id => $user->user_id,
				gpg_key => $key->blob
			      });
		    }
		    $self->{_sy_update} = 1;
		} else {
		    $self->stash('error_msg', "Invalid GPG key: $mesg");
		}
	    }
	}
    }
    $self->render;
}

sub resume {
    my $self = shift;
    if ($self->req->method eq 'POST') {
	my $user = $self->stash('user');
	if ($self->param('update_resume')) {
	    $user->resume->resume($self->param('resume'));
	    $user->resume->update;
	} else {
	    foreach my $skl ($user->skills->all) {
		if ($self->param('update_skill_'.$skl->user_skill_id)) {
		    $self->db->resultset('UserSkill')
			->find({ user_skill_id => $skl->user_skill_id })
			->update({
			    skill_level_id => $self->param('level'),
			    skill_experience_id => $self->param('experience')
			  });
		    last;
		}
		if ($self->param('del_'.$skl->user_skill_id)) {
		    $self->db->resultset('UserSkill')
			->find({ user_skill_id => $skl->user_skill_id })
			->delete;
		    last;
		}
	    }
	    if ($self->param('add_skill')) {
		my $skill_id = $self->param('new_skill');
		if ($skill_id == -1) {
		    $self->stash('error_msg', "Please select a skill");
		} else {
		    try {
			$self->db->resultset('UserSkill')
			    ->create({
				user_id => $user->user_id,
				skill_id => $skill_id,
				skill_level_id => $self->param('new_level'),
				skill_experience_id => $self->param('new_experience')
			      });
		    } catch {
			use DBIx::Class::Exception;
			if ($_ =~ /Duplicate entry/) {
			    $self->stash('error_msg', 'Duplicate skill');
			} else {
			    $self->app->log->error($_);
			    $self->stash('error_msg',
					 'Database error occurred');
			}
		    }
		}
	    }
	}
    }
    $self->render;
}

1;
