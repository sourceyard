package Sourceyard::Log::Syslog;
use strict;
use warnings;
use Mojo::Base 'Mojo::Log';
use File::Basename 'basename';
use Sys::Syslog qw(:standard :macros);
use Carp;

sub new {
    my $class = shift;
    local %_ = @_;
    my $tag = delete $_{tag} || basename($0);
    my $facility = delete $_{facility} || LOG_USER;
    my $level = delete $_{level} || 'debug';
    croak "unrecognized arguments" if keys(%_);
    openlog($tag, "ndelay,pid", $facility);
    return $class->SUPER::new(level => $level);
}

sub debug { shift->_syslog(debug => LOG_DEBUG => @_) }
sub warn  { shift->_syslog(warn  => LOG_WARN  => @_) }
sub info  { shift->_syslog(info  => LOG_INFO  => @_) }
sub error { shift->_syslog(error => LOG_ERROR => @_) }
sub fatal { shift->_syslog(fatal => LOG_CRIT  => @_) }

sub _syslog {
    my ($self, $level, $prio) = (shift, shift, shift);
    $self->{_priority} = $prio;
    $self->${\ "SUPER::$level"}(@_);
}

sub append {
    my ($self, $msg) = @_;
    syslog($self->{_priority}, "%s", $msg);
}

has format => sub { shift->short ? \&_fmt_short : \&_fmt_long };

sub _fmt_short {
    my (undef, undef, @lines) = @_;
    return join("\n", @lines);
}

sub _fmt_long {
    my (undef, $level, @lines) = @_;
    return "[$level]: ".join("\n", @lines);
}



1;
