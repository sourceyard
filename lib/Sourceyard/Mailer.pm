package Sourceyard::Mailer;
use parent 'Mail::Send';
use strict;
use warnings;
use Data::Dumper;

sub new {
    my ($class, $ctl) = (shift, shift);
    local %_ = @_;
    $_{from} //= $ctl->config->get(qw(mail sender_address));
    my $self = $class->SUPER::new(%_);
    $self->add('User-Agent', __PACKAGE__);
    $self->add("X-Sourceyard-Server", $ctl->config->get(qw(core domain)));
    $self->controller($ctl);
    return $self;
}

sub controller {
    my ($self, $ctl) = @_;
    if ($ctl) {
	$self->{_controller} = $ctl;
    }
    return $self->{_controller};
}

sub from {
    my ($self, $addr) = @_;
    $self->add("From", $addr);	
}

sub send {
    my ($self, $tmpl, %stash) = @_;
    my $fd = $self->open;
    my $body = $self->controller->render_to_string($tmpl, format => 'txt',
						   %stash);
    print $fd $body;
    close $fd;
    return $self;
}

1;

    
