package Sourceyard::Schema;

use strict;
use warnings;

use base 'DBIx::Class::Schema';

use Sourceyard::Config;

our $VERSION = 1;

__PACKAGE__->load_namespaces;

sub DBI_arg {
    my ($self, $cfg) = @_;

    $cfg = new Sourceyard::Config unless defined $cfg;
    
    my $dbi = 'DBI:mysql';
    my $v;

    if ($v = $cfg->get(qw(database name))) {
	$dbi .= ":database=$v";
    }
    if ($v = $cfg->get(qw(database host))) {
	$dbi .= ":host=$v";
    }
    if ($v = $cfg->get(qw(database port))) {
	$dbi .= ":port=$v";
    }
    if ($v = $cfg->get(qw(database config-file))) {
	$dbi .= ":;mysql_read_default_file=$v";
    }

    return $dbi unless wantarray;
    return ($dbi,
	    $cfg->get(qw(database user)),
	    $cfg->get(qw(database password)));
}   

sub connect {
    my ($self, $cfg) = @_;
    $self->SUPER::connect($self->DBI_arg($cfg));
}

1;
