package Sourceyard::Schema::Result::Authorized_Keys;

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->table('authorized_keys');
__PACKAGE__->add_columns(
    key_id => {
	data_type => 'integer',
	is_auto_increment => 1,
    },    
    user_id => {
	data_type => 'integer',
    },
    ssh_key => {
	data_type => 'text',
    }
);
__PACKAGE__->set_primary_key('key_id');
__PACKAGE__->belongs_to('user_id' => 'Sourceyard::Schema::Result::User');

1;
