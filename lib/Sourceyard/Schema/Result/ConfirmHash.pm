package Sourceyard::Schema::Result::ConfirmHash;

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components(qw/ InflateColumn::DateTime Core /);
__PACKAGE__->table('confirm_hash');
__PACKAGE__->add_columns(
    hash_id => {
	data_type => 'integer',
	is_auto_increment => 1,
    },    
    user_id => {
	data_type => 'integer',
    },
    confirm_hash => {
	data_type => 'varchar',
	size => 128,
    },
    date => {
	data_type => 'timestamp',
	default_value => \ 'CURRENT_TIMESTAMP'
    }    
);

__PACKAGE__->set_primary_key('hash_id');
__PACKAGE__->add_unique_constraint(['user_id']);
__PACKAGE__->add_unique_constraint(['confirm_hash']);
__PACKAGE__->belongs_to('user_id' => 'Sourceyard::Schema::Result::User');

    
