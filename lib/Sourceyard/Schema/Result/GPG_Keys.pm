package Sourceyard::Schema::Result::GPG_Keys;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use App::Sourceyard::GPG::PublicKeySet;
use Carp;

__PACKAGE__->table('gpg_keys');
__PACKAGE__->add_columns(
    key_id => {
	data_type => 'integer',
	is_auto_increment => 1,
    },    
    user_id => {
	data_type => 'integer',
    },
    gpg_key => {
	data_type => 'text',
    }
);
__PACKAGE__->set_primary_key('key_id');
__PACKAGE__->belongs_to('user_id' => 'Sourceyard::Schema::Result::User');

__PACKAGE__->inflate_column('gpg_key', {
    inflate => sub {
	my ($value, $object) = @_;
	my $pks = new App::Sourceyard::GPG::PublicKeySet($value);
	if ($pks->status == PKS_SYSERR) {
	    croak "system error: " . join("\n", $pks->error);
	}
	if ($pks->status == PKS_INVALID) {
	    return new App::Sourceyard::GPG::PublicKey(
		type => 'pub',
		blob => $object
	    );
	}
	if ($pks->status == PKS_EMPTY) {
	    return new App::Sourceyard::GPG::PublicKey(
		type => 'pub',
		blob => ''
	    );
	}
	shift @{[$pks->pubkeys()]};
    },
    deflate => sub {
	my ($value, $object) = @_;
	return $value->blob;
    }
});	 


1;
