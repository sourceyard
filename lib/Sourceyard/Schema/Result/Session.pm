package Sourceyard::Schema::Result::Session;

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components(qw/ InflateColumn::DateTime Core /);
__PACKAGE__->table('session');
__PACKAGE__->add_columns(
    session_id => {
	data_type => 'integer',
	is_auto_increment => 1,
    },    
    user_id => {
	data_type => 'integer',
    },
    session_key => {
	data_type => 'varchar',
	size => 128,
    },
    created_on => {
	data_type => 'timestamp',
	default_value => \ 'CURRENT_TIMESTAMP'
    },
    expires_on => {
	data_type => 'timestamp'
    }
);

__PACKAGE__->set_primary_key('session_id');
__PACKAGE__->add_unique_constraint(['session_key']);
__PACKAGE__->belongs_to('user_id' => 'Sourceyard::Schema::Result::User');
    
    
