package Sourceyard::Schema::Result::Skill;

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->table('skill');
__PACKAGE__->add_columns(
    skill_id => {
	data_type => 'integer',
	is_auto_increment => 1,
    },
    name => {
	data_type => 'varchar',
	size => 255
    }
);
__PACKAGE__->set_primary_key('skill_id');
__PACKAGE__->add_unique_constraint(['name']);
# __PACKAGE__->belongs_to('skill_id' => 'Sourceyard::Schema::Result::UserSkill',
# 			'skill_id');


	    

