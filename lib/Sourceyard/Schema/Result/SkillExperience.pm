package Sourceyard::Schema::Result::SkillExperience;

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->table('skill_experience');
__PACKAGE__->add_columns(
    skill_experience_id => {
	data_type => 'integer',
	is_auto_increment => 1,
    },
    name => {
	data_type => 'varchar',
	size => 255
    }
);
__PACKAGE__->set_primary_key('skill_experience_id');
__PACKAGE__->add_unique_constraint(['name']);
# __PACKAGE__->belongs_to('skill_experience_id' => 'Sourceyard::Schema::Result::UserSkill',
# 			'skill_experience_id');


	    

