package Sourceyard::Schema::Result::User;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use App::Sourceyard::Password;

__PACKAGE__->load_components(qw(
            InflateColumn::DateTime
            InflateColumn::App::Sourceyard::Enum
            Core
 ));
__PACKAGE__->table('user');
__PACKAGE__->add_columns(
    user_id => {
	data_type => 'integer',
	is_auto_increment => 1,
    },
    user_name => {
	data_type => 'varchar',
	size => 33
    },
    email => {
	data_type => 'varchar',
	size => 255
    },
    user_pw => {
	data_type => 'varchar',
	size => 128
    },
    real_name => {
	data_type => 'varchar',
	size => 128
    },
    status => {
	data_type => 'varchar',
	size => 1,
	enum => 'Sourceyard::User::Status',
	default_value => 'P'
    },
    system_uid => {
	data_type => 'integer',
	is_nullable => 1
    },
    spamscore => {
	data_type => 'integer',
    },
    add_date => {
	data_type => 'timestamp',
	default_value => \ 'CURRENT_TIMESTAMP'
    },
    timezone => {
	data_type => 'varchar',
	size => 64,
	default_value => 'UTC'
    },
    theme => {
	data_type => 'varchar',
	size => 32,
	default_value => 'Savannah'
    }
);
__PACKAGE__->set_primary_key('user_id');
__PACKAGE__->add_unique_constraint(['user_name']);
__PACKAGE__->add_unique_constraint(['user_id','user_name']);
__PACKAGE__->add_unique_constraint(['email']);
__PACKAGE__->has_many(
    gpg_keys => 'Sourceyard::Schema::Result::GPG_Keys',
    { 'foreign.user_id' => 'self.user_id' }
);
__PACKAGE__->has_many(
    authorized_keys => 'Sourceyard::Schema::Result::Authorized_Keys',
    { 'foreign.user_id' => 'self.user_id' }
);
__PACKAGE__->might_have(
    resume => 'Sourceyard::Schema::Result::UserResume',
    { 'foreign.user_id' => 'self.user_id' }
);    
__PACKAGE__->might_have(
    confirm_hash => 'Sourceyard::Schema::Result::ConfirmHash',
    { 'foreign.user_id' => 'self.user_id' }
);    
__PACKAGE__->has_many(
    skills => 'Sourceyard::Schema::Result::UserSkill',
    { 'foreign.user_id' => 'self.user_id' }
);
__PACKAGE__->has_many(
    session => 'Sourceyard::Schema::Result::Session',
    { 'foreign.user_id' => 'self.user_id' }
);    

__PACKAGE__->inflate_column('user_pw', {
    inflate => sub {
	my ($value, $object) = @_;
	return new App::Sourceyard::Password(encrypted => $value);
    },
    deflate => sub {
	my ($value, $object) = @_;
	return $value->crypt;
    }
});

1;
