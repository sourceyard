package Sourceyard::Schema::Result::UserResume;

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->table('user_resume');
__PACKAGE__->add_columns(
    resume_id => {
	data_type => 'integer',
	is_auto_increment => 1,
    },    
    user_id => {
	data_type => 'integer',
    },
    resume => {
	data_type => 'text',
    },
    spamscore => {
	data_type => 'integer'
    }
);

__PACKAGE__->set_primary_key('resume_id');
__PACKAGE__->add_unique_constraint(['user_id']);
__PACKAGE__->belongs_to('user_id' => 'Sourceyard::Schema::Result::User');

    
