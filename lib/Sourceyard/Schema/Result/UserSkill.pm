package Sourceyard::Schema::Result::UserSkill;

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->table('user_skill');
__PACKAGE__->add_columns(
    user_skill_id => {
	data_type => 'integer',
	is_auto_increment => 1,
    },
    user_id => {
	data_type => 'integer',
    },
    skill_id => {
	data_type => 'integer',
    },
    skill_level_id => {
	data_type => 'integer',
    },
    skill_experience_id => {
	data_type => 'integer',
    }
);
__PACKAGE__->set_primary_key('user_skill_id');
__PACKAGE__->add_unique_constraint([qw(user_id skill_id)]);
__PACKAGE__->has_one(skill => 'Sourceyard::Schema::Result::Skill',
		     { 'foreign.skill_id' => 'self.skill_id' },
		     { cascade_delete => 0 });
__PACKAGE__->has_one(skill_level => 'Sourceyard::Schema::Result::SkillLevel',
		     { 'foreign.skill_level_id' => 'self.skill_level_id' },
		     { cascade_delete => 0 });
__PACKAGE__->has_one(skill_experience => 'Sourceyard::Schema::Result::SkillExperience',
		     { 'foreign.skill_experience_id' => 'self.skill_experience_id' },
		     { cascade_delete => 0 });
    
