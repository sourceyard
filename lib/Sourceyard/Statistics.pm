package Sourceyard::Statistics;

use strict;
use warnings;

use parent 'Exporter';

sub new {
    my ($class, $ctl) = @_;
    return bless { _db => $ctl->db }, $class;
}

sub db {
    my ($self) = @_;
    return $self->{_db};
}

sub users {
    my ($self, $status) = @_;
    $status ||= 'A';
    unless ($self->{_users}{$status}) {
	$self->{_users}{$status} =
	    $self->db->resultset('User')
	         ->search({ status => $status })
		 ->count;
    }	
    return $self->{_users}{$status};
}
   

1;
