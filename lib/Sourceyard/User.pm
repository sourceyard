package Sourceyard::User;

use strict;
use warnings;
use parent qw(Sourceyard::Schema::Result::User);
use Carp;
use Email::Address::XS;
use Net::DNS;
use Crypt::Cracklib;
use App::Sourceyard::Password;
use App::Sourceyard::GPG::PublicKeySet;
use Net::SSH::Perl::Key;
use Try::Tiny;
use Data::Rand::Obscure;
use DateTime;
use DateTime::Duration;
use Sourceyard::User::Status;

# new(controller, username, [status])
# new(controller, template)
# new(controller, { cond })

sub new {
    my ($class, $c, $arg) = (shift, shift, shift);
    my $cond;

    croak "bad number of arguments" if !(defined($c) && defined($arg));
    if (ref($arg) eq 'HASH') {
	$cond = $arg;
    } elsif ($arg->isa('App::Sourceyard::User::Template')) {
	croak "bad number of arguments" if @_;
	croak "template not valid" unless $arg->validate;
	try {
	    $c->db->resultset('User')->create({
		user_name => $arg->username,
		email => $arg->email,
		user_pw => $arg->password,
		real_name => $arg->realname,
		status => Sourceyard::User::Status->pending,
		confirm_hash => {
		    confirm_hash => Data::Rand::Obscure->new->create_hex(length=>64)
		}
	    });
	} catch {
	    $c->app->log->error("failed to create user ".$arg->username.": $_");
	    return undef;
	};
	$cond = {
	    user_name => $arg->username,
	    status => Sourceyard::User::Status->pending
	};
    } else {
	$cond = {
	    user_name => $arg,
	    status => shift // Sourceyard::User::Status->active
	};
	croak "bad number of arguments" if @_;
    }
    my $obj = $c->db->resultset('User')->single($cond);
    return undef unless $obj;
    my $self = bless $obj, $class;
    $self->controller($c);
    return $self;
}

sub controller {
    my ($self, $c) = @_;
    if ($c) {
	$self->{_controller} = $c;
    }
    return $self->{_controller};
}

sub confirm_hash {
    my ($self, $create) = @_;
    if ($create) {
	my $hash = Data::Rand::Obscure->new->create_hex(length => 64); 
	return $self->controller->db->resultset('ConfirmHash')
	            ->update_or_create({ user_id => $self->user_id,
					 confirm_hash => $hash,
				         date => DateTime->now },
				       { user_id => $self->user_id });
    } else {
	my $confirm_hash = $self->SUPER::confirm_hash;
	if ($confirm_hash &&
	    (DateTime->now->epoch - $confirm_hash->date->epoch) >
	        $self->controller
	             ->config->get(qw(register confirmation-ttl))) {
	    $confirm_hash->delete;
	    $self->discard_changes;
	    return undef;
	}
	return $confirm_hash;
    }
}

sub create_session {
    my $self = shift;
    local %_ = @_;
    my $hash = Data::Rand::Obscure->new->create_hex(length => 64);
    my $now = DateTime->now;
    my $ttl = $_{ttl} || $self->controller->config->get(qw(session ttl));
    my $exp = $now + new DateTime::Duration(seconds => $ttl);
    my $ret = $self->controller->db->resultset('Session')
	           ->create({ user_id => $self->user_id,
			      session_key => $hash,
			      created_on => $now,
			      expires_on => $exp
			    },
			    { user_id => $self->user_id });
    $self->controller->session(session_key => $ret->session_key,
			       expiration => $ttl);
    $self->discard_changes;
    return $ret;
}

sub validate_real_name {
    my ($self, $name) = @_;
    if (split(/\s+/, $name) > 1) {
	return $name;
    }
    return undef;
}

sub validate_email {
    my ($self, $email) = @_;

    if ($email =~ m{^<(.+)>$}) {
	$email = $1; # dequote
    }
    
    my $addr;
    eval {
	$addr = Email::Address::XS->new(address => $email);
    };
    return undef if $@ || !defined($addr->user) || !defined($addr->host);

    my $resolver = new Net::DNS::Resolver();
    my $reply = $resolver->query($addr->host, 'MX');
    if (!$reply || $reply->answer == 0) {
	return undef;
    }

    return $addr->address;
}

sub validate_password {
    my ($self, $pass, $dref) = @_;
    my $res = bad_pass($pass);
    if ($res ne '') {
	$$dref = $res if defined($dref);
	return undef;
    }
    if (!$self->{_dry_run} && $self->user_pw->check($pass)) {
	$$dref = 'Password the same';
	return undef;
    }
    return new App::Sourceyard::Password($pass);
}

sub validate_ssh_key {
    my ($self, $key) = @_;

    $key =~ s/\n//;
    $key =~ s/^\s+//;
    $key =~ s/\s+$//;

    my @in = split /\s+/, $key;
    my $type = shift @in;
    my @block;
    while (my $frag = shift @in) {
	if ($frag =~ m{^[A-Za-z0-9+/=]+$}) {
	    push @block, $frag;
	    $key = $type . ' ' . join('', @block) . ' ' . join(' ', @in);
	    return $key if
		eval { Net::SSH::Perl::Key->extract_public(undef, $key) };
	}
    }

    return undef;
}

sub validate_gpg_key {
    my ($self, $key, $dref) = @_;
    my $pk = new App::Sourceyard::GPG::PublicKeySet($key);
    unless ($pk->status == PKS_OK) {
	if ($dref) {
	    $$dref = join("\n", $pk->error);
	}
	return undef;
    }
    return $pk;
}
1;
