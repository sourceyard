package Sourceyard::User::Status;
use App::Sourceyard::Enum (
    'Sourceyard::User::Status' => {
	active    => 'A',
	pending   => 'P',
	suspended => 'S',
	deleted   => 'D'
    }
);
1;
