package Sourceyard::User::Template;
use parent 'App::Sourceyard::User::Template';
use strict;
use warnings;
use Carp;

sub new {
    my $class = shift;
    my $ctl = shift or croak 'not enough arguments';
    my $self = $class->SUPER::new;
    $self->{_controller} = $ctl;
    $self->set(@_);
    return $self;
}

sub controller {
    my ($self) = @_;
    return $self->{_controller};
}

sub user {
    my ($self) = @_;
    return $self->controller->stash('user');
}

sub _validate_username {
    my ($self) = @_;
    my $res = $self->SUPER::_validate_username();
    if ($res) {
	if ($self->controller
	         ->db
	         ->resultset('User')
	         ->single({ user_name => $self->username })) {
	    $self->error(username => 'Username already exists');
	    return 0;
	}
    }
    return $res;
}

sub _validate_password {
    my ($self) = @_;
    my $res = $self->SUPER::_validate_password();
    if ($res && $self->user && $self->user->user_pw->check($self->password)) {
	$self->error(password => "Password didn't change");
	return 0;
    }
    return $res;
}
    
sub _validate_email {
    my ($self) = @_;
    my $res = $self->SUPER::_validate_email;
    if ($res && $self->controller
	             ->db
	             ->resultset('User')
	             ->single({ email => $self->email })) {
	$self->error(email => "A user with this email already exists");
	return 0;
    }
    return $res;
}

1;
