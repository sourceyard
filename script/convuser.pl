use strict;
use warnings;

use FindBin;
BEGIN { unshift @INC, "$FindBin::Bin/../lib" }

use Sourceyard::Schema;
use Savane::Schema;
use Sourceyard::User::Status;

my $inscm = Savane::Schema->connect;
my $outscm = Sourceyard::Schema->connect;

foreach my $s ($inscm->resultset('PeopleSkill')->all) {
    $outscm->resultset('Skill')->create({
	skill_id => $s->skill_id,
	name => $s->name
    });
}
foreach my $s ($inscm->resultset('PeopleSkillLevel')->all) {
    $outscm->resultset('SkillLevel')->create({
	skill_level_id => $s->skill_level_id,
	name => $s->name
    });
}
foreach my $s ($inscm->resultset('PeopleSkillYear')->all) {
    $outscm->resultset('SkillExperience')->create({
	skill_experience_id => $s->skill_year_id,
	name => $s->name
    });
}

my $savane_users = $inscm->resultset('User')->search({ status => 'A' });

while (my $user = $savane_users->next) {
    my %addset;
    my $v;
    
    if ($v = $user->gpg_key) {
	$addset{gpg_keys} = [ { gpg_key => $v } ];
    }
    if ($v = $user->authorized_keys) {
	$addset{authorized_keys} = [
	    map { { ssh_key => $_ } } split /###/, $v
	];
    }
    if ($v = $user->people_resume) {
	$addset{resume} = { resume => $v,
			    spamscore => $user->resume_spamscore };
    }
    if (($v = $user->confirm_hash) && length($v) >= 32) {
    	$addset{confirm_hash} = { confirm_hash => $v };
    }
    
    my $sy_user = $outscm->resultset('User')->create({
	user_id => $user->user_id,
	user_name => $user->user_name,
	email => $user->email,
	user_pw => $user->user_pw,
	real_name => $user->realname,
	status => new Sourceyard::User::Status($user->status),
	system_uid => $user->uidnumber,
	spamscore => $user->spamscore,
	add_date => \ ( 'from_unixtime('.$user->add_date.')' ), 
	timezone => $user->timezone,
	theme => $user->theme,
	skills => [
	    map {
		{ user_id  => $_->user_id,
		  skill_id => $_->skill_id,
		  skill_level_id => $_->skill_level_id,
		  skill_experience_id => $_->skill_year_id }
	    } $user->skills->all
	],
	%addset
    });
}

