use strict;
use warnings;

use FindBin;
BEGIN { unshift @INC, "$FindBin::Bin/../lib" }

use Sourceyard::Schema;

my $schema = Sourceyard::Schema->connect;
$schema->deploy({ add_drop_table => 1 });
