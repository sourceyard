package TestConfig;

use strict;
use Carp;
use File::Temp;

use App::Sourceyard::Config qw(:sort);
use parent 'App::Sourceyard::Config';

sub new {
    my $class = shift;
    my $text;
    local %_ = @_;
    
    my $file = new File::Temp(UNLINK => 1);
    if (defined($text = delete $_{text})) {
	print $file $text;
    } else {
	while (<main::DATA>) {
	    print $file $_;
	}
    }
    close $file;

    my $exp = delete $_{expect};
    my $self = $class->SUPER::new($file->filename, %_);
    $self->{_expected_errors} = $exp if $exp;
    $self->{_status} = $self->parse();
    if ($exp && @{$self->{_expected_errors}}) {
	$self->{_status} = 0;
	$self->error("not all expected errors reported");
    }
    return $self;
}

sub success {
    my ($self) = @_;
    return $self->{_status};
}

sub canonical {
    my $self = shift;
    local %_ = @_;
    my $delim;
    unless (defined($delim = delete $_{delim})) {
	$delim = " ";
    }
    carp "unknown parameters: " . join(', ', keys(%_)) if (keys(%_));
    return undef unless $self->success;
    
    return join $delim, map {
	local $Data::Dumper::Useqq = 1;
	local $Data::Dumper::Terse = 1;
	local $Data::Dumper::Indent = 0;
	join('.', @{$_->[0]}) . "=" . Data::Dumper->Dump([$_->[1]->value]);
    } $self->flatten(sort => SORT_PATH);
}

sub expected_error {
    my ($self, $msg) = @_;

    if (exists($self->{_expected_errors})) {
	my ($i) = grep { ${$self->{_expected_errors}}[$_] eq $msg }
	             0..$#{$self->{_expected_errors}};
	if (defined($i)) {
	    splice(@{$self->{_expected_errors}}, $i, 1);
	    return 1;
	}
    }
}

sub error {
    my $self = shift;
    my $err = shift;
    local %_ = @_;

    if (exists($_{locus})) {
	push @{$self->{_errors}}, { message => $err };
	print STDERR $_{locus}->format($err)."\n"
	    unless $self->expected_error($err);
    } else {
	push @{$self->{_errors}}, { message => $err };
	print STDERR "$err\n"
    }
}

sub errors {
    my $self = shift;
    return undef if $self->success;
    return @{$self->{_errors}};
}

sub lint {
    my $self = shift;
    my $synt = shift;
    local %_ = @_;
    my $exp = $self->{_expected_errors} = delete $_{expect};
    carp "unknown parameters: " . join(', ', keys(%_)) if (keys(%_));
    
    my $ret = $self->SUPER::lint($synt);

    if ($exp && @{$self->{_expected_errors}}) {
	$self->{_status} = 0;
	$self->error("not all expected errors reported");
    }
    return $ret;
}

1;
