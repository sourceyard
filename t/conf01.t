# -*- perl -*-
use lib qw(t lib);
use strict;
use Test;
use TestConfig;

plan(tests => 1);

my $cfg = new TestConfig;
ok($cfg->canonical, 'backend.foo.file="a" core.retain-interval=10 core.tempdir="/tmp"');

__DATA__
# This is a sample configuration file    
[core]
	retain-interval = 10
	tempdir = /tmp
[backend foo]
	file = a
    
    
