# -*- perl -*-
use lib qw(t lib);
use strict;
use Test;
use TestConfig;

plan(tests => 7);

my %keywords = (
    core => {
	section => {
	    'retain-interval' => { mandatory => 1 },
	    'tempdir' => 1,
	    'verbose' => 1,
	}
    },
    backend => {
	section => {
	    '*' => {
		section => {
		    file => 1
		}
	    }
	}
    }
);

my $cfg = new TestConfig(parameters => \%keywords);
ok($cfg->is_set('backend','foo','file'));
ok($cfg->is_variable('backend','foo','file'));
ok($cfg->get('backend','foo','file'), 'foo');

ok($cfg->is_set('core', 'verbose') == 0);

ok($cfg->is_section('backend','foo'));

$cfg->set('core','verbose','On');
ok($cfg->get('core','verbose'),'On');

$cfg->unset('core','tmpdir');
ok($cfg->is_set('core','tmpdir') == 0);

__DATA__
# This is a sample configuration file    
[core]
	retain-interval = 10
	tempdir = /tmp
[backend foo]
	file = foo
