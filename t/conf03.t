# -*- perl -*-

use lib qw(t lib);
use strict;
use Test;
use TestConfig;

plan(tests => 1);

my %keywords = (
    core => {
	section => {
	    'tempdir' => 1,
	    'verbose' => 1,
	}
    },
    backend => {
	section => {
	    file => 1
	}
    }
);
my $cfg = new TestConfig(parameters => \%keywords,
                         expect => [ 'keyword "output" is unknown' ]);
ok($cfg->errors() == 1);
__DATA__
# This is a sample configuration file    
[core]
	tempdir = /tmp
	output = file

