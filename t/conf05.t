# -*- perl -*-
use lib qw(t lib);
use strict;
use Test;
use TestConfig;

plan(tests => 1);

my %keywords = (
    core => {
	section => {
	    list => {
		array => 1
	    },
	    pidfile => 1
	}
    }
);

my $cfg = new TestConfig(parameters => \%keywords);
ok($cfg->canonical(),'core.list=["en","to",5] core.pidfile="file2"');

__END__
[core]
    list = en
    list = to
    list = 5

    pidfile = file1
    pidfile = file2
