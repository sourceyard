# -*- perl -*-
use lib qw(t lib);
use strict;
use Test;
use TestConfig;
use Data::Dumper;

plan(tests => 2);

my %keywords = (
    core => {
	section => {
	    'retain-interval' => { mandatory => 1 },
	    'tempdir' => 1,
	    'verbose' => 1,
	}
    },
    '*' => '*'
);

my $cfg = new TestConfig(parameters => \%keywords);
ok($cfg->canonical, 'backend.file.level=3 backend.file.local=1 core.retain-interval=10 core.tempdir="/tmp"');

my %subkw = (
    backend => {
	section => {
	    file => {
		section => {
		    name => { mandatory => 1 },
		    local => 1
		}
	    }
	}
    }
);

ok(!$cfg->lint(\%subkw,
	   expect => [ 'keyword "level" is unknown',
		       'mandatory variable "backend.file.name" not set' ]));

__DATA__
# This is a sample configuration file    
[core]
	retain-interval = 10
	tempdir = /tmp
[backend file]
        local = 1
        level = 3
