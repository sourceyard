# -*- perl -*-
use lib qw(t lib);
use strict;
use Test;
use TestConfig;

plan(tests => 1);

my %keywords = (
    base => { mandatory => 1 },
    file => { default => sub {
	my $self = shift;
	return $self->get('base') . '/passwd';
      }
    }
);

my $t = new TestConfig(parameters => \%keywords);
ok($t->get('file'), '/etc/passwd');

__DATA__
base = /etc
