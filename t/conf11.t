# -*- perl -*-
use lib qw(t lib);
use strict;
use Test;
use TestConfig;

plan(tests => 3);

my $cfg = new TestConfig;
ok(join(',', $cfg->getnode('core')->keys), 'retain-interval,tempdir');
ok($cfg->getnode('core')->keys, 2);
ok(join(',', sort $cfg->names_of('core')), 'retain-interval,tempdir');

__DATA__
# This is a sample configuration file    
[core]
	retain-interval = 10
	tempdir = /tmp
