# -*- perl -*-
use strict;
use lib qw(t lib);
use Test;

plan tests => 14;

use App::Sourceyard::Enum (
    'My::Color' => {
	black => 0,
	white => 1,
	red   => 2,
    });
    
my $c = new My::Color;
ok(!defined($c->value));
ok(!$c->is_black);
ok(!$c->is_white);
ok(!$c->is_red);

$c->set_black;
ok($c->is_black);
ok(!$c->is_white);
ok(!$c->is_red);

$c->set_white;
ok(!$c->is_black);
ok($c->is_white);
ok(!$c->is_red);

$c->set_red;
ok(!$c->is_black);
ok(!$c->is_white);
ok($c->is_red);

ok("$c", 2);
