# -*- perl -*-
use strict;
use lib qw(t lib);
use Test;

plan tests => 5;

use App::Sourceyard::Enum (
    'My::Color' => {
	black => 0,
	white => 1,
	red   => 2,
    });

my $c = new My::Color(2);
ok($c->is_red);

$c->value(1);
ok($c->is_white);

$c->unset;
ok(!$c->is_black);
ok(!$c->is_white);
ok(!$c->is_red);
