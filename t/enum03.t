# -*- perl -*-
use strict;
use lib qw(t lib);
use Test;

plan tests => 1;

use App::Sourceyard::Enum (
    'My::Color' => {
	black => 0,
	white => 1,
	red   => 2,
    },
    default => 'white');

my $c = new My::Color;
ok($c->is_white);
