# -*- perl -*-
use strict;
use lib qw(t lib);
use Test;

plan tests => 2;

use App::Sourceyard::Enum (
    'My::Color' => {
	black => 0,
	white => 1,
	red   => 2,
    },
    default => 'black');

my $c = My::Color->white;
ok($c->is_white);

eval { $c->set_red };
ok($c->is_white);
