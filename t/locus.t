# -*- perl -*-
use strict;
use lib qw(t lib);
use Test;
use App::Sourceyard::Config::Locus;

plan(tests => 14);

my $loc = new App::Sourceyard::Config::Locus;
ok($loc->format, '');
ok($loc->format('test', 'message'), 'test message');

$loc->add('foo', 10);
ok($loc->format, "foo:10");
ok("$loc", "foo:10");

$loc->add('foo', 11);
$loc->add('foo', 12);
$loc->add('foo', 13);
ok($loc->format, "foo:10-13");

$loc->add('foo', 24);
$loc->add('foo', 28);
ok($loc->format, "foo:10-13,24,28");
ok($loc->format('test', 'message'), "foo:10-13,24,28: test message");

$loc->add('bar', 1);
$loc->add('baz', 8);
$loc->add('baz', 9);
$loc->add('bar', 5);
ok($loc->format, "foo:10-13,24,28;bar:1,5;baz:8-9");

$loc->fixup_names('foo' => 'Foo', 'bar' => 'BAR');
ok($loc->format, "Foo:10-13,24,28;BAR:1,5;baz:8-9");

$loc->fixup_lines('Foo' => -1, 'baz' => 2);
ok($loc->format, "Foo:9-12,23,27;BAR:1,5;baz:10-11");

$loc->fixup_lines(3);
ok($loc->format, "Foo:12-15,26,30;BAR:4,8;baz:13-14");

$loc = new App::Sourceyard::Config::Locus("foo", 10, 15);
ok("$loc", "foo:10,15");

$loc += "bar:11";
ok("$loc", "foo:10,15;bar:11");

$loc = new App::Sourceyard::Config::Locus("foo", 10);
my $res = "bar:1" + $loc;
ok("$res", "bar:1;foo:10");
