# -*- perl -*-
use lib qw(t lib);
use strict;
use Test;
use App::Sourceyard::Password;

plan(tests => 2);

my $pass = new App::Sourceyard::Password('guessme');
ok($pass->check('guessme'));
ok(!$pass->check('Guessme'));
