# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test;
use App::Sourceyard::User::Template;
use Data::Dumper;

plan tests => 4;

my $t = new App::Sourceyard::User::Template;
ok($t);

ok(!$t->validate_username);

#$t->username('gray');
#ok(!$t->validate_username);
#ok($t->error('username'), 'Username already exists');

$t->username('user');
ok($t->validate_username);
ok(!$t->validate);

