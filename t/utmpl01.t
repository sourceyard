# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test;
use App::Sourceyard::User::Template;

plan tests => 4;

my $t = new App::Sourceyard::User::Template();
ok($t);

$t->realname('John');
ok(!$t->validate_realname);

$t->realname('John Smith');
ok($t->validate_realname);
ok(!$t->validate);
