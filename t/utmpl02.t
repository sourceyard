# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test;
use App::Sourceyard::User::Template;
use Data::Dumper;

plan tests => 7;

my $t = new App::Sourceyard::User::Template();
ok($t);

$t->password('foo');
ok(!$t->validate_password);
ok($t->error('password'), 'it is WAY too short');

$t->password('opendoor');
ok(!$t->validate_password);
ok($t->error('password'), 'it is based on a dictionary word');

$t->password('r3GuR5afEr');
ok($t->validate_password);

ok(!$t->validate);

