# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test;
use App::Sourceyard::User::Template;

plan tests => 9;

my $t = new App::Sourceyard::User::Template();
ok($t);
ok(!$t->validate_email);
ok($t->error('email'), 'not defined');

$t->email('emailaddress');
ok(!$t->validate_email);
ok($t->error('email'), 'required part missing');

$t->email('foo@nosuchaddress.exists.anywhere');
ok(!$t->validate_email);
ok($t->error('email'), 'invalid domain');

$t->email('gray@gnu.org');
ok($t->validate_email);
ok(!$t->validate);
