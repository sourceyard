# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test;
use App::Sourceyard::User::Template;

plan tests => 2;

my $t = new App::Sourceyard::User::Template(
    username => 'guest',
    realname => 'John Smith',
    email => 'foo@gmail.com',
    password => 'r3GuR5afEr');
ok($t);
ok($t->validate);
